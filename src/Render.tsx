import * as ReactDOM from 'react-dom'
import { App } from './layouts/App'
import './fonts.css'

function render() {
  ReactDOM.render(<App />, document.getElementById('root'))
}

render()
