import { StorageData, StorageDataKey, StorageSetDataReturn } from '@typings/storageData.typing'
import ElectronStore from 'electron-store'

export class AppStore {
  private static storeData = new ElectronStore<StorageData>({
    defaults: {
      workspaceList: [
        {
          id: 'default',
          name: 'Default'
        }
      ],
      projectList: [],
      managerVersion: {
        npm: {
          available: [],
          downloaded: [],
          versionDataList: []
        }
      },
      systemManagers: [],
      gitAccessTokenList: [],
      runConfigurationList: [],
      windowSize: {
        height: 600,
        width: 800
      },
      gitPath: '',
      currentWorkspaceId: 'default'
    }
  })

  static getStoreData<K extends StorageDataKey>(key: K): StorageData[K] {
    return this.storeData.get(key)
  }

  static setStoreData<K extends StorageDataKey>(key: K, fn: StorageSetDataReturn<K>) {
    if (typeof fn === 'function') {
      const d = this.getStoreData(key)
      const res = fn(d)
      return this.storeData.set(key, res)
    }
    this.storeData.set(key, fn)
  }

  static resetData(key: StorageDataKey) {
    this.storeData.reset(key)
  }
}
