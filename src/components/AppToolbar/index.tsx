import { PropsWithChildren } from 'react'
import { AppBar, Toolbar } from '@mui/material'

interface AppToolbarProps {}

export const AppToolbar = ({ children }: PropsWithChildren<AppToolbarProps>) => {
  return (
    <AppBar sx={{ position: 'relative' }}>
      <Toolbar sx={{ backgroundColor: 'primary.main' }}>{children}</Toolbar>
    </AppBar>
  )
}
