import { Paper, PaperProps, Typography } from '@mui/material'
import { PropsWithChildren } from 'react'

interface BoxFrameProps {
  label: string
  paperProps?: PaperProps
}

const BoxFrame = ({ label, children, paperProps }: PropsWithChildren<BoxFrameProps>) => {
  return (
    <Paper
      variant={'outlined'}
      sx={{
        py: 3,
        px: 2
      }}
      style={{ position: 'relative' }}
      {...paperProps}
    >
      <Typography
        variant={'subtitle2'}
        sx={{
          position: 'absolute',
          top: -12,
          left: 10,
          backgroundColor: 'background.paper',
          color: 'gray',
          px: 1
        }}
      >
        {label}
      </Typography>
      {children}
    </Paper>
  )
}

export default BoxFrame
