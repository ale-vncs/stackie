import { forwardRef, PropsWithChildren, ReactElement, Ref } from 'react'
import { Box, Dialog, IconButton, Slide, Typography } from '@mui/material'
import { FaTimes } from 'react-icons/fa'
import { TransitionProps } from '@mui/material/transitions'
import { AppToolbar } from '@components/AppToolbar'

export interface FullScreenDialog {
  open: boolean
  title: string
  onClose: () => void
  disablePaddingTop?: boolean
}

const Transition = forwardRef(function Transition(
  props: TransitionProps & {
    children: ReactElement
  },
  ref: Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />
})

export default function FullScreenDialog({
  title,
  open,
  onClose,
  disablePaddingTop,
  children
}: PropsWithChildren<FullScreenDialog>) {
  return (
    <Dialog fullScreen open={open} TransitionComponent={Transition}>
      <AppToolbar>
        <IconButton edge="start" color="inherit" onClick={onClose} aria-label="close">
          <FaTimes />
        </IconButton>
        <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
          {title}
        </Typography>
      </AppToolbar>
      <Box sx={{ pt: disablePaddingTop ? 0 : 2, display: 'flex', overflow: 'auto', height: '100%' }}>{children}</Box>
    </Dialog>
  )
}
