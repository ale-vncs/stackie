import { ButtonProps, Dialog, DialogActions, DialogContent, DialogProps, DialogTitle, Typography } from '@mui/material'
import { PropsWithChildren } from 'react'
import { CustomButton } from '@components/input'
import { LoadingScreen } from '@components/LoadingScreen'

type DialogButtonProps = Omit<ButtonProps, 'id' | 'onClick'>

interface SimpleDialogProps extends DialogProps {
  open: boolean
  onClose: () => void
  onConfirm?: () => void
  confirmLabel?: string
  cancelLabel?: string
  confirmBtnProps?: DialogButtonProps
  cancelBtnProps?: DialogButtonProps
  loading?: boolean
}

export const SimpleDialog = ({
  title,
  onClose,
  onConfirm,
  children,
  cancelBtnProps,
  confirmBtnProps,
  cancelLabel,
  confirmLabel,
  loading,
  ...props
}: PropsWithChildren<SimpleDialogProps>) => {
  return (
    <Dialog {...props}>
      <LoadingScreen loading={loading ?? false} />
      {title && (
        <DialogTitle sx={{ px: 1 }} component={Typography}>
          {title}
        </DialogTitle>
      )}
      <DialogContent sx={{ px: 1, pt: '8px !important' }}>{children}</DialogContent>
      <DialogActions>
        <CustomButton id={'secondary-btn'} variant={'outlined'} onClick={onClose} {...cancelBtnProps}>
          {cancelLabel || 'Cancelar'}
        </CustomButton>
        <CustomButton id={'primary-btn'} variant={'contained'} onClick={onConfirm} {...confirmBtnProps}>
          {confirmLabel || 'Confirmar'}
        </CustomButton>
      </DialogActions>
    </Dialog>
  )
}
