import React from 'react'
import { useController, FieldValues } from 'react-hook-form'
import { ComponentDefaultProps } from './useFormElement'
import { zodMessageParse } from '@utils/zodMessageParse.util'
import { CustomAutoComplete } from '@components/input'
import { CustomAutoCompleteProps } from '@components/input/CustomAutoComplete'

interface FormAutoCompleteProps<T, E extends FieldValues>
  extends ComponentDefaultProps<CustomAutoCompleteProps<T>, E> {}

const FormAutoComplete = <T, E extends FieldValues>({ name, control, ...props }: FormAutoCompleteProps<T, E>) => {
  const { field, fieldState } = useController({
    control,
    name
  })

  const error = fieldState.error && zodMessageParse(fieldState.error)

  return (
    <CustomAutoComplete
      {...props}
      ref={field.ref}
      onBlur={field.onBlur}
      onChangeValue={field.onChange}
      helperText={error}
      value={(field.value as any) ?? (props.multiple ? ([] as any) : null)}
    />
  )
}

export default FormAutoComplete
