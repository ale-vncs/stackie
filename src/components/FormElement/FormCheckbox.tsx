import React, { ReactNode } from 'react'
import { ComponentDefaultProps } from './useFormElement'
import { useController, FieldValues } from 'react-hook-form'
import { CustomCheckBox } from '../input/CustomCheckBox'

interface FormCheckboxFieldProps {
  id: string
  label: ReactNode
  handleChange?: (value: boolean) => void
  disabled?: boolean
}

type FormCheckboxProps<E extends FieldValues> = ComponentDefaultProps<FormCheckboxFieldProps, E>

const FormCheckbox = <E extends FieldValues>({
  id,
  name,
  control,
  label,
  handleChange,
  disabled
}: FormCheckboxProps<E>) => {
  const { field } = useController({
    control,
    name
  })

  const handleChangeCheck = (value: boolean) => {
    field.onChange(value)
    if (handleChange) handleChange(value)
  }

  return (
    <CustomCheckBox
      id={id}
      label={label}
      checked={field.value as boolean | undefined}
      onChange={(e) => handleChangeCheck(e)}
      name={field.name}
      disabled={disabled}
    />
  )
}

export default FormCheckbox
