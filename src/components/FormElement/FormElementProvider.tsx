import { PropsWithChildren } from 'react'
import { FormProvider, FieldValues } from 'react-hook-form'
import { FormElementReturn } from './useFormElement'

export interface FormElementProviderProps<T extends FieldValues> {
  formMethods: FormElementReturn<T>
}

const FormElementProvider = <T extends FieldValues>({
  children,
  formMethods
}: PropsWithChildren<FormElementProviderProps<T>>) => {
  return (
    <FormProvider
      clearErrors={formMethods.clearErrors}
      control={formMethods.control}
      formState={formMethods.formState}
      getValues={formMethods.getValues}
      handleSubmit={formMethods.submit}
      register={formMethods.register}
      reset={formMethods.reset}
      setError={formMethods.setError}
      setFocus={formMethods.setFocus}
      setValue={formMethods.setValue}
      trigger={formMethods.trigger}
      unregister={formMethods.unregister}
      watch={formMethods.watch}
      getFieldState={formMethods.getFieldState}
      resetField={formMethods.resetField}
    >
      {children}
    </FormProvider>
  )
}
export default FormElementProvider
