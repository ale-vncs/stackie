import { ComponentDefaultProps } from './useFormElement'
import CustomNumberField, { CustomNumberFieldProps } from '../input/CustomNumberField'
import { useController, FieldValues } from 'react-hook-form'
import { zodMessageParse } from '@utils/zodMessageParse.util'

type FormNumberFieldProps<E extends FieldValues> = ComponentDefaultProps<CustomNumberFieldProps, E>

const FormNumberField = <E extends FieldValues>({ control, name, ...props }: FormNumberFieldProps<E>) => {
  const { field, fieldState } = useController({ control, name })
  const error = fieldState.error

  return (
    <CustomNumberField
      {...props}
      name={field.name}
      inputRef={field.ref}
      onChangeNumber={field.onChange}
      onBlur={field.onBlur}
      value={Number(props.value) || Number(field.value)}
      error={!!error}
      helperText={error ? zodMessageParse(error) : props.helperText}
    />
  )
}

export default FormNumberField
