import { ComponentDefaultProps } from './useFormElement'
import { useController, FieldValues } from 'react-hook-form'
import { zodMessageParse } from '@utils/zodMessageParse.util'
import { CustomNumberNullField } from '../input'
import { CustomNumberNullFieldProps } from '../input/CustomNumberNullField'

type FormNumberNullFieldProps<E extends FieldValues> = ComponentDefaultProps<CustomNumberNullFieldProps, E>

const FormNumberNullField = <E extends FieldValues>({ control, name, ...props }: FormNumberNullFieldProps<E>) => {
  const { field, fieldState } = useController({ control, name })
  const error = fieldState.error

  return (
    <CustomNumberNullField
      {...props}
      name={field.name}
      inputRef={field.ref}
      onChangeNumber={field.onChange}
      onBlur={field.onBlur}
      value={field.value as number | null}
      error={!!error}
      helperText={error ? zodMessageParse(error) : props.helperText}
    />
  )
}

export default FormNumberNullField
