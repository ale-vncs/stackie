import React from 'react'
import { ComponentDefaultProps } from './useFormElement'
import { useController, FieldValues } from 'react-hook-form'
import { CustomRadioGroup, CustomRadioGroupProps } from '../input/CustomRadioGroup'
import { zodMessageParse } from '@utils/zodMessageParse.util'

type FormRadioGroupProps<E extends FieldValues> = ComponentDefaultProps<CustomRadioGroupProps, E>

const FormRadioGroup = <E extends FieldValues>({ name, control, ...props }: FormRadioGroupProps<E>) => {
  const { field, fieldState } = useController({
    control,
    name
  })

  const error = fieldState.error

  return (
    <CustomRadioGroup
      {...props}
      value={field.value as string | null}
      onChange={field.onChange}
      name={field.name}
      ref={field.ref}
      error={!!error}
      helperText={error ? zodMessageParse(error) : ''}
    />
  )
}

export default FormRadioGroup
