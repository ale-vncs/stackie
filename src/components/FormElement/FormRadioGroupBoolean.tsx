import React from 'react'
import { ComponentDefaultProps } from './useFormElement'
import { useController, FieldValues } from 'react-hook-form'
import CustomRadioGroupBoolean, { CustomRadioGroupBooleanProps } from '../input/CustomRadioGroupBoolean'

type FormRadioGroupProps<E extends FieldValues> = ComponentDefaultProps<CustomRadioGroupBooleanProps, E>

const FormRadioGroupBoolean = <E extends FieldValues>({ name, control, ...props }: FormRadioGroupProps<E>) => {
  const { field } = useController({
    control,
    name
  })

  return (
    <CustomRadioGroupBoolean
      {...props}
      value={field.value as boolean}
      onChange={field.onChange}
      name={field.name}
      ref={field.ref}
    />
  )
}

export default FormRadioGroupBoolean
