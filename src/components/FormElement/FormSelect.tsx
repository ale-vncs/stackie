import React from 'react'
import { useController, FieldValues } from 'react-hook-form'
import { ComponentDefaultProps } from './useFormElement'
import { zodMessageParse } from '@utils/zodMessageParse.util'
import CustomSelect, { CustomSelectProps } from '../input/CustomSelect'
import { Null } from '@typings/generic.typing'

type FormSelectProps<E extends FieldValues> = ComponentDefaultProps<CustomSelectProps, E>

const FormSelect = <E extends FieldValues>({ name, control, options, ...props }: FormSelectProps<E>) => {
  const { field, fieldState } = useController({
    control,
    name
  })

  const error = fieldState.error

  return (
    <CustomSelect
      {...props}
      options={options}
      name={field.name}
      inputRef={field.ref}
      onChangeText={field.onChange}
      onBlur={field.onBlur}
      value={(field.value as Null<string> | undefined) || null}
      error={!!error}
      helperText={error ? zodMessageParse(error) : props.helperText}
    />
  )
}

export default FormSelect
