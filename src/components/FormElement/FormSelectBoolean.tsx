import React, { useCallback } from 'react'
import { useController, FieldValues } from 'react-hook-form'
import { ComponentDefaultProps } from './useFormElement'
import { zodMessageParse } from '@utils/zodMessageParse.util'
import CustomSelect, { CustomSelectProps } from '../input/CustomSelect'

interface FormSelectBooleanProps<E extends FieldValues>
  extends ComponentDefaultProps<Omit<CustomSelectProps, 'options'>, E> {
  textSim?: string
  textNao?: string
}

const FormSelectBoolean = <E extends FieldValues>({
  textNao,
  textSim,
  name,
  control,
  ...props
}: FormSelectBooleanProps<E>) => {
  const { field, fieldState } = useController({
    control,
    name
  })

  const error = fieldState.error

  const onChangeValue = useCallback((value: string) => {
    field.onChange(value === 'sim')
  }, [])

  const getValue = () => {
    switch (field.value as boolean | null) {
      case true:
        return 'sim'
      case false:
        return 'nao'
      default:
        return null
    }
  }

  return (
    <CustomSelect
      {...props}
      options={[
        {
          label: textSim || 'Sim',
          value: 'sim'
        },
        {
          label: textNao || 'Não',
          value: 'nao'
        }
      ]}
      name={field.name}
      inputRef={field.ref}
      onChangeText={(t) => onChangeValue(t)}
      onBlur={field.onBlur}
      value={getValue()}
      error={!!error}
      helperText={error ? zodMessageParse(error) : props.helperText}
    />
  )
}

export default FormSelectBoolean
