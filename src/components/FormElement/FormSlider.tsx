import { ComponentDefaultProps } from './useFormElement'
import { CustomSliderProps } from '../input/CustomSlider'
import { CustomSlider } from '../input'
import { useController, FieldValues } from 'react-hook-form'

interface FormSliderFieldProps extends Omit<CustomSliderProps, 'onChangeValue' | 'value'> {}

type FormSliderProps<E extends FieldValues> = ComponentDefaultProps<FormSliderFieldProps, E>

const FormSlider = <E extends FieldValues>({ name, control, ...props }: FormSliderProps<E>) => {
  const { field } = useController({ control, name })

  return (
    <CustomSlider
      {...props}
      value={field.value as number | number[] | undefined}
      onChangeValue={field.onChange}
      name={field.name}
      onBlur={field.onBlur}
    />
  )
}

export default FormSlider
