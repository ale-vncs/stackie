import React from 'react'
import { ComponentDefaultProps } from './useFormElement'
import { useController, FieldValues } from 'react-hook-form'
import { CustomSwitch } from '../input/CustomSwitch'
import { FormControlProps } from '@mui/material'

interface FormSwitchFieldProps {
  id: string
  selectedLabel: string
  notSelectedLabel: string
  formControlProps?: FormControlProps
  title: string
  handleChange?: (value: boolean) => void
}

type FormSwitchProps<E extends FieldValues> = ComponentDefaultProps<FormSwitchFieldProps, E>

const FormSwitch = <E extends FieldValues>({
  id,
  name,
  control,
  selectedLabel,
  notSelectedLabel,
  title,
  formControlProps,
  handleChange
}: FormSwitchProps<E>) => {
  const { field } = useController({ control, name })
  const handleChangeCheck = (value: boolean) => {
    field.onChange(value)
    if (handleChange) handleChange(value)
  }

  return (
    <CustomSwitch
      id={id}
      title={title}
      checked={field.value as boolean}
      selectedLabel={selectedLabel}
      notSelectedLabel={notSelectedLabel}
      formControl={formControlProps}
      onChange={handleChangeCheck}
      formControlLabel={{ name: field.name }}
    />
  )
}

export default FormSwitch
