import React from 'react'
import { useController, FieldValues } from 'react-hook-form'
import { zodMessageParse } from '@utils/zodMessageParse.util'
import { ComponentDefaultProps } from './useFormElement'
import { CustomTextField, CustomTextFieldProps } from '../input/CustomTextField'
import { Null } from '@typings/generic.typing'

export type FormTextFieldProps<E extends FieldValues> = ComponentDefaultProps<CustomTextFieldProps, E>

const FormTextField = <E extends FieldValues>({ name, control, ...props }: FormTextFieldProps<E>) => {
  const { field, fieldState } = useController({
    control,
    name
  })

  const error = fieldState.error

  return (
    <CustomTextField
      {...props}
      name={field.name}
      inputRef={field.ref}
      onChangeText={field.onChange}
      onBlur={field.onBlur}
      value={props.value || (field.value as Null<string> | undefined)}
      error={!!error}
      helperText={error ? zodMessageParse(error) : props.helperText}
    />
  )
}

export default FormTextField
