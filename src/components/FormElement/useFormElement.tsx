import { useEffect } from 'react'
import {
  Control,
  DefaultValues,
  Path,
  useForm,
  KeepStateOptions,
  UseFormSetValue,
  UseFormGetValues,
  UseFormReset,
  UseFormWatch,
  UseFormTrigger,
  UseFormRegister,
  UseFormSetError,
  UseFormClearErrors,
  UseFormSetFocus,
  UseFormResetField,
  UseFormGetFieldState,
  FormState,
  FieldValues,
  UseFormUnregister
} from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import { z } from '@utils/zodMessageParse.util'

interface FormProps<T> {
  validation: z.ZodSchema<T>
  defaultValues: DefaultValues<T>
  values?: T
  validationOnStart?: boolean
  validationOnReset?: boolean
}

export type ComponentDefaultProps<T, E extends FieldValues> = T & {
  name: Path<E>
  control: Control<E>
}

export interface FormElementReturn<T extends FieldValues> {
  formState: FormState<T>
  setValue: UseFormSetValue<T>
  getValues: UseFormGetValues<T>
  control: Control<T>
  reset: UseFormReset<T>
  submit: (cb: (data: T) => void) => () => Promise<void>
  watch: UseFormWatch<T>
  trigger: UseFormTrigger<T>
  register: UseFormRegister<T>
  setError: UseFormSetError<T>
  clearErrors: UseFormClearErrors<T>
  setFocus: UseFormSetFocus<T>
  resetField: UseFormResetField<T>
  getFieldState: UseFormGetFieldState<T>
  unregister: UseFormUnregister<T>
}

const useFormElement = <T extends FieldValues>({
  defaultValues,
  validation,
  values,
  validationOnStart = false,
  validationOnReset = true
}: FormProps<T>): FormElementReturn<T> => {
  const methods = useForm<T>({
    resolver: zodResolver(validation),
    defaultValues,
    values
  })
  const {
    control,
    handleSubmit,
    formState,
    reset,
    trigger,
    setValue,
    getValues,
    watch,
    setError,
    register,
    clearErrors,
    setFocus,
    resetField,
    getFieldState,
    unregister
  } = methods

  useEffect(() => {
    if (control) {
      reset(defaultValues)
      if (validationOnStart) trigger().then()
    }
  }, [])

  const onReset = (values?: DefaultValues<T>, keepStateOptions?: KeepStateOptions) => {
    const defaultVal = values || defaultValues
    reset(defaultVal, keepStateOptions)
    if (validationOnReset) trigger()
  }

  const onSetValue: UseFormSetValue<T> = (name, value, options) => {
    setValue(name, value, options)
  }

  return {
    formState,
    setValue: onSetValue,
    getValues,
    control,
    reset: onReset,
    submit: handleSubmit,
    watch,
    trigger,
    register,
    setError,
    clearErrors,
    setFocus,
    resetField,
    getFieldState,
    unregister
  } as FormElementReturn<T>
}

export default useFormElement
