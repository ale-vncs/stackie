import { FrameScreen } from '@components/FrameScreen/index'
import { PropsWithChildren } from 'react'
import { FaAngleLeft } from 'react-icons/fa'
import { CustomButton } from '@components/input'
import { FrameScreenProps } from '@components/FrameScreen/FrameScreen'

export interface BackFrameScreenProps extends Omit<FrameScreenProps, 'componentBeforeTitle'> {
  onClickBack: () => void
}

export const BackFrameScreen = ({ onClickBack, children, ...frameProps }: PropsWithChildren<BackFrameScreenProps>) => {
  return (
    <FrameScreen
      {...frameProps}
      componentBeforeTitle={
        <CustomButton
          id={'b3a69ee3-f25d-4e8d-9761-c68ce6e4570d'}
          variant={'outlined'}
          color={'inherit'}
          onClick={onClickBack}
          startIcon={<FaAngleLeft />}
        >
          Voltar
        </CustomButton>
      }
    >
      {children}
    </FrameScreen>
  )
}
