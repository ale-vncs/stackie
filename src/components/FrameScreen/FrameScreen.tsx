import { Box, Typography } from '@mui/material'
import { PropsWithChildren, ReactNode } from 'react'
import { AppToolbar } from '@components/AppToolbar'

export interface FrameScreenProps {
  title: string
  componentBeforeTitle?: ReactNode
  disablePaddingTop?: boolean
}

export const FrameScreen = ({
  title,
  componentBeforeTitle,
  disablePaddingTop,
  children
}: PropsWithChildren<FrameScreenProps>) => {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        width: '100%',
        backgroundColor: 'background.paper'
      }}
    >
      <AppToolbar>
        {componentBeforeTitle}
        <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
          {title}
        </Typography>
      </AppToolbar>
      <Box sx={{ pt: disablePaddingTop ? 0 : 2, display: 'flex', overflow: 'auto', height: '100%' }}>{children}</Box>
    </Box>
  )
}
