import { Box, CircularProgress, Stack, Typography } from '@mui/material'

export interface LoadingScreenProps {
  loading: boolean
}

export const LoadingScreen = ({ loading }: LoadingScreenProps) => {
  if (!loading) return null
  return (
    <Box
      sx={{
        position: 'absolute',
        zIndex: 1900,
        width: '100%',
        height: '100%',
        top: 0,
        left: 0,
        background: 'black',
        opacity: 0.6
      }}
    >
      <Stack
        direction={'column'}
        justifyContent={'center'}
        alignItems={'center'}
        rowGap={2}
        width={'100%'}
        height={'100%'}
      >
        <CircularProgress size={20} />
        <Typography>Carregando...</Typography>
      </Stack>
    </Box>
  )
}
