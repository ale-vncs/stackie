import { Box } from '@mui/material'
import { PropsWithChildren, useMemo, useState } from 'react'

type Size = number

export interface ResizerProps {
  onResize?: (x: number, y: number) => void
  size: [Size, Size]
  minConstraint?: [number, number]
  maxConstraint?: [number, number]
}

export const Resizer = ({
  onResize,
  minConstraint,
  maxConstraint,
  size,
  children
}: PropsWithChildren<ResizerProps>) => {
  const [divSize, setDivSize] = useState({
    x: 0,
    y: 0
  })

  const childrenMemo = useMemo(() => children, [children])

  return <Box sx={{ position: 'relative' }}>{childrenMemo}</Box>
}
