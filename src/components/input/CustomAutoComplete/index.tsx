import React from 'react'
import {
  Autocomplete,
  AutocompleteProps,
  AutocompleteRenderInputParams,
  CircularProgress,
  TextField
} from '@mui/material'
import { TextFieldProps as TFProps } from '@mui/material/TextField'

type Value<T> = T | NonNullable<string | T> | (string | T)[] | null | undefined

export interface CustomAutoCompleteProps<T>
  extends Omit<AutocompleteProps<T, boolean | undefined, boolean | undefined, boolean | undefined>, 'renderInput'> {
  id: string
  renderInput?: (params: AutocompleteRenderInputParams) => React.ReactNode
  TextFieldProps?: TFProps
  onChangeValue: (option: Value<T>) => void
  label?: string
  selectKey?: (option: T) => string | number
  helperText?: string
}

const CustomAutoComplete = <T,>({
  id,
  value,
  onChangeValue,
  renderInput,
  label,
  TextFieldProps,
  selectKey,
  helperText,
  ...props
}: CustomAutoCompleteProps<T>) => {
  const renderOption = selectKey
    ? (propsLi: React.HTMLAttributes<HTMLLIElement>, option: T) => {
        return (
          <li {...propsLi} key={selectKey(option)}>
            {props.getOptionLabel ? props.getOptionLabel(option) : option}
          </li>
        )
      }
    : undefined

  const defaultInput = (params: AutocompleteRenderInputParams) => (
    <TextField
      {...params}
      size={'small'}
      label={label || 'Selecione'}
      variant="outlined"
      inputProps={{
        ...params.inputProps,
        autoComplete: 'off'
      }}
      error={!!helperText}
      helperText={helperText && helperText}
      {...TextFieldProps}
      InputProps={{
        ...params.InputProps,
        endAdornment: (
          <React.Fragment>
            {props.loading ? <CircularProgress color="primary" size={20} /> : null}
            {params.InputProps.endAdornment}
          </React.Fragment>
        )
      }}
      sx={{
        '& .MuiOutlinedInput-root.MuiInputBase-sizeSmall': {
          paddingRight: '39px'
        }
      }}
    />
  )
  return (
    <Autocomplete<T, boolean | undefined, boolean | undefined, boolean | undefined>
      id={id}
      size={'small'}
      autoHighlight
      fullWidth
      noOptionsText={'Sem opção'}
      loadingText={'Carregando...'}
      renderInput={renderInput || defaultInput}
      renderOption={renderOption}
      {...props}
      onChange={(_, value) => onChangeValue(value as Value<T>)}
      value={(value as Value<T>) || null}
    />
  )
}

export default CustomAutoComplete
