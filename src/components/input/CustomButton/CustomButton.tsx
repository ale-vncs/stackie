import React from 'react'
import { Button, ButtonProps, CircularProgress } from '@mui/material'

export interface CustomButtonProps extends ButtonProps {
  id: string
  isLoading?: boolean
}

const CustomButton = ({ isLoading, ...props }: CustomButtonProps) => {
  return (
    <Button size={'small'} {...props} disabled={isLoading || props.disabled}>
      {isLoading ? <CircularProgress size={22} /> : props.children}
    </Button>
  )
}

export default CustomButton
