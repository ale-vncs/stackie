import { CircularProgress, IconButton, IconButtonProps } from '@mui/material'
import { IconBaseProps, IconType } from 'react-icons'
import { forwardRef } from 'react'

export interface CustomIconButtonProps extends IconButtonProps {
  id: string
  icon: IconType
  iconProps?: IconBaseProps
  loading?: boolean
  buttonSize?: 'small' | 'medium' | 'large'
}

const CustomIconButton = forwardRef<HTMLButtonElement, CustomIconButtonProps>(function CustomIconButton(
  { iconProps, icon: Icon, buttonSize, loading, ...iconButtonProps },
  ref
) {
  return (
    <IconButton
      ref={ref}
      {...iconButtonProps}
      size={buttonSize || 'medium'}
      disabled={loading || iconButtonProps.disabled}
    >
      {loading ? <CircularProgress size={iconProps?.size || 16} /> : <Icon size={16} {...iconProps} />}
    </IconButton>
  )
})

export default CustomIconButton
