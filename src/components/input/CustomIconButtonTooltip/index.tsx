import CustomIconButton, { CustomIconButtonProps } from '../CustomIconButton'
import { IconType } from 'react-icons'
import { Tooltip, TooltipProps } from '@mui/material'
import React from 'react'

export interface CustomIconButtonTooltipProps extends CustomIconButtonProps {
  icon: IconType
  onClick: (e?: unknown) => void
  title: string
  placement?: TooltipProps['placement']
  buttonSize?: 'small' | 'medium' | 'large'
  open?: boolean
  arrow?: boolean
  tooltipProps?: Omit<TooltipProps, 'children' | 'title'>
}

const CustomIconButtonTooltip = ({
  icon,
  onClick,
  open,
  placement = 'top',
  buttonSize,
  title,
  arrow = true,
  tooltipProps,
  ...iconProps
}: CustomIconButtonTooltipProps) => {
  return (
    <Tooltip {...tooltipProps} title={title} open={open} placement={placement} arrow={arrow}>
      <span>
        <CustomIconButton icon={icon} onClick={onClick} {...iconProps} buttonSize={buttonSize} />
      </span>
    </Tooltip>
  )
}

export default CustomIconButtonTooltip
