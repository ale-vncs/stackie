import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { debounce, InputAdornment, TextField, Typography } from '@mui/material'
import { TextFieldProps } from '@mui/material/TextField'
import { Null } from '@typings/generic.typing'

export interface CustomNumberFieldProps extends Omit<TextFieldProps, 'value' | 'onChange'> {
  id: string
  onChangeNumber?: (value: number) => void
  value?: Null<number>
  currency?: boolean
  percent?: boolean
  digit?: number
  format?: boolean
  readOnly?: boolean
  max?: number
  maxLength?: number
  startIcon?: React.ReactNode
  endIcon?: React.ReactNode
  mask?: string
  debounceTime?: number
}

const CustomNumberField = ({
  id,
  onChangeNumber,
  value,
  currency = false,
  percent = false,
  digit = 0,
  readOnly = false,
  format = false,
  max,
  maxLength = 15,
  endIcon,
  startIcon,
  mask,
  debounceTime = 50,
  ...textFieldProps
}: CustomNumberFieldProps) => {
  const [numberValue, setNumberValue] = useState('0')
  const debounceNumber = useCallback(
    !debounceTime ? (n: number) => onChangeNumber?.(n) : debounce((n: number) => onChangeNumber?.(n), debounceTime),
    [onChangeNumber, debounceTime]
  )
  const digits = useMemo(() => {
    if (!mask) {
      return {
        integer: 1,
        decimal: currency ? 2 : digit || 0
      }
    }
    const [auxInteger, auxDecimal] = mask.split(',')
    return {
      integer: !auxInteger.length ? 1 : auxInteger.length,
      decimal: !auxDecimal ? 0 : auxDecimal.length
    }
  }, [mask])

  if (currency) digit = 2

  const formatDecimalNumber = (value: string) => {
    return (Number(value) / Math.pow(10, digit)).toString()
  }

  const maxValue = (value: string) => {
    if (max !== undefined) {
      return Number(value) > max ? max.toString() : value
    }
  }

  const truncateValue = (value: string) => {
    if (maxLength === 0) return 0
    if (maxLength) {
      const truncateValue = maxLength > 15 ? 15 : maxLength
      return value.length >= truncateValue ? value.slice(0, truncateValue) : value
    }
  }

  const formater = (value: string | undefined) => {
    if (!value) return value
    return Intl.NumberFormat('pt-BR', {
      minimumIntegerDigits: digits.integer,
      minimumFractionDigits: digits.decimal
    }).format(Number(value))
  }

  const currencyFormater = (value: string | undefined) => {
    const formater = Intl.NumberFormat('pt-Br', {
      style: 'currency',
      currency: 'BRL'
    })
    return formater.format(Number(value) || 0).replace('R$', '')
  }

  const renderNumber = () => {
    if (currency) return currencyFormater(numberValue)
    if (format || mask) return formater(numberValue)
    return numberValue
  }

  const onChangeInput = (value: string) => {
    let result = value.replace(/\D+/g, '')
    result = String(Number(result))
    if (maxLength) result = truncateValue(result) as string
    if (digit >= 2 || currency) result = formatDecimalNumber(result)
    if (max !== undefined) result = maxValue(result) as string
    debounceNumber(Number(result))
    setNumberValue(result)
  }

  const getCurrentPrefix = () => {
    return currency ? (
      <InputAdornment position="start">
        <Typography>R$</Typography>
      </InputAdornment>
    ) : (
      startIcon
    )
  }

  const getPercentPrefix = () => {
    return percent ? (
      <InputAdornment position="end">
        <Typography>%</Typography>
      </InputAdornment>
    ) : (
      endIcon
    )
  }

  useEffect(() => {
    if (value === 0 && numberValue === '0') return
    if (value || value === 0) return setNumberValue(value?.toString())
  }, [value, numberValue])

  return (
    <TextField
      id={id}
      size={'small'}
      variant={'outlined'}
      value={renderNumber()}
      onChange={(e) => onChangeInput(e.target.value)}
      InputProps={{
        autoComplete: 'off',
        startAdornment: getCurrentPrefix(),
        endAdornment: getPercentPrefix()
      }}
      inputProps={{
        readOnly,
        style: readOnly ? { cursor: 'default' } : undefined
      }}
      {...textFieldProps}
    />
  )
}

export default CustomNumberField
