import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { debounce, InputAdornment, TextField, Typography } from '@mui/material'
import { TextFieldProps } from '@mui/material/TextField'
import { Null } from '@typings/generic.typing'

interface CustomNumberNullFieldBaseProps extends Omit<TextFieldProps, 'value' | 'onChange'> {
  id: string
  onChangeNumber?: (value: number | null) => void
  value?: number | null
  currency?: boolean
  percent?: boolean
  format?: boolean
  readOnly?: boolean
  max?: number
  maxLength?: number
  startIcon?: React.ReactNode
  endIcon?: React.ReactNode
  notNull?: boolean
}

interface CustomNumberNullFieldMaskProps extends CustomNumberNullFieldBaseProps {
  mask: string
}

interface CustomNumberNullFieldDigitProps extends CustomNumberNullFieldBaseProps {
  digit: number
}

interface CustomNumberNullFieldCurrencyProps extends CustomNumberNullFieldBaseProps {
  currency: boolean
}

export type CustomNumberNullFieldProps =
  | CustomNumberNullFieldBaseProps
  | CustomNumberNullFieldCurrencyProps
  | CustomNumberNullFieldDigitProps
  | CustomNumberNullFieldMaskProps

interface NumberState {
  numberValue: Null<number>
  displayNumber: string
}

const initialState: NumberState = { numberValue: null, displayNumber: '' }

const CustomNumberNullField = ({
  id,
  onChangeNumber,
  value,
  percent = false,
  format = false,
  readOnly = false,
  max,
  maxLength = 15,
  endIcon,
  startIcon,
  notNull = false,
  ...textFieldProps
}: CustomNumberNullFieldProps) => {
  const [numberState, setNumberState] = useState<NumberState>(initialState)
  const debounceNumber = useCallback(
    debounce((d: number | null) => onChangeNumber?.(d), 50),
    [onChangeNumber]
  )
  if (maxLength > 15) maxLength = 15

  const formatData = useMemo(() => {
    let integer = 1
    let decimal = 0
    const currency = 'currency' in textFieldProps
    if (currency) {
      integer = 1
      decimal = 2
    }
    if ('mask' in textFieldProps) {
      const decimalMask = textFieldProps.mask.split(',')
      integer = decimalMask[0].length
      decimal = decimalMask[1] ? decimalMask[1].length : 0
    }
    if ('digit' in textFieldProps) {
      decimal = textFieldProps.digit
    }
    return {
      currency,
      integer,
      decimal
    }
  }, [textFieldProps])

  const formatter = (value: number) => {
    const { decimal, integer } = formatData
    const semFormatacao = !format && !decimal
    if (semFormatacao) return value.toString()
    return Intl.NumberFormat('pt-BR', {
      minimumIntegerDigits: integer,
      minimumFractionDigits: decimal
    }).format(value)
  }

  const maxFormatter = (value: number) => {
    if (max !== undefined) {
      return value > max ? max : value
    }
    return 0
  }

  const truncatFormatter = (value: number) => {
    if (maxLength) {
      const strigValue = value.toString()
      const slicedValue = strigValue.length >= maxLength ? strigValue.slice(0, maxLength) : value
      return Number(slicedValue)
    }
    return 0
  }

  const decimalFormatter = (value: number) => {
    const { decimal } = formatData
    return value / Math.pow(10, decimal)
  }

  const formatterResult = (value: string) => {
    const repeatCharacter = formatData.decimal + formatData.integer - 1
    if ((!notNull && value === '0'.repeat(repeatCharacter) && numberState.numberValue === 0) || value === '') {
      setNumberState(initialState)
      debounceNumber?.(null)
      return
    }
    const { decimal } = formatData
    let numericResult = Number(value)
    if (maxLength) numericResult = truncatFormatter(numericResult)
    if (max) numericResult = maxFormatter(numericResult)
    if (decimal > 0) numericResult = decimalFormatter(numericResult)
    setNumberState({
      numberValue: numericResult,
      displayNumber: formatter(numericResult)
    })
    debounceNumber?.(numericResult)
  }

  const onChangeInput = (value: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const stringResult = value.target.value.replace(/\D+/g, '')
    formatterResult(stringResult)
  }

  useEffect(() => {
    const numberValue = notNull ? Number(value) : value
    if (numberValue !== numberState.numberValue) {
      if (numberValue === null || numberValue === undefined) {
        setNumberState(initialState)
      } else {
        setNumberState({
          numberValue,
          displayNumber: formatter(numberValue)
        })
      }
    }
  }, [value])

  const getCurrentPrefix = () => {
    if (formatData.currency) {
      return (
        <InputAdornment position="start">
          <Typography>R$</Typography>
        </InputAdornment>
      )
    }
    return startIcon
  }

  const getPercentPrefix = () => {
    return percent ? (
      <InputAdornment position="end">
        <Typography>%</Typography>
      </InputAdornment>
    ) : (
      endIcon
    )
  }

  return (
    <TextField
      id={id}
      size={'small'}
      variant={'outlined'}
      value={numberState.displayNumber}
      onChange={onChangeInput}
      InputProps={{
        autoComplete: 'off',
        startAdornment: getCurrentPrefix(),
        endAdornment: getPercentPrefix(),
        readOnly,
        style: readOnly ? { cursor: 'default', pointerEvents: 'none' } : undefined
      }}
      {...textFieldProps}
    />
  )
}

export default CustomNumberNullField
