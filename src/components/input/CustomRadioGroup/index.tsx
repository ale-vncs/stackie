import React from 'react'
import RadioGroup, { RadioGroupProps } from '@mui/material/RadioGroup'
import FormControlLabel, { FormControlLabelProps } from '@mui/material/FormControlLabel'
import Radio, { RadioProps } from '@mui/material/Radio'
import { FormControl, FormHelperText } from '@mui/material'

export interface RadioLabelValueProps {
  label: string
  value: string
  radioProps?: Omit<RadioProps, 'size' | 'color'>
  formControlLabelProps?: Omit<FormControlLabelProps, 'control' | 'label'>
}

export interface CustomRadioGroupProps extends Omit<RadioGroupProps, 'onChange' | 'value'> {
  id: string
  onChange?: (value: string) => void
  value?: string | null
  error?: boolean
  helperText?: string
  radios: RadioLabelValueProps[]
}

export const CustomRadioGroup = ({
  id,
  onChange,
  value,
  radios,
  helperText,
  error,
  ...props
}: CustomRadioGroupProps) => {
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange?.(e.target.value)
  }
  return (
    <FormControl>
      <RadioGroup id={id} onChange={handleChange} value={value} {...props}>
        {radios.map((rd, index) => {
          return (
            <FormControlLabel
              id={`${id}--form-control-label--${index}`}
              control={<Radio id={`${id}--radio--${index}`} size={'small'} color={'primary'} {...rd.radioProps} />}
              label={rd.label}
              value={rd.value}
              key={`${rd.label}-${rd.value}`}
              {...rd.formControlLabelProps}
            />
          )
        })}
      </RadioGroup>
      {error && helperText && <FormHelperText error>{helperText}</FormHelperText>}
    </FormControl>
  )
}

export default CustomRadioGroup
