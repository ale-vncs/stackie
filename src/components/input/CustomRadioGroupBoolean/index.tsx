import React from 'react'
import { RadioGroupProps } from '@mui/material/RadioGroup'
import { FormControlLabel, RadioGroup } from '@mui/material'
import Radio from '@mui/material/Radio'

export interface CustomRadioGroupBooleanProps extends Omit<RadioGroupProps, 'onChange' | 'value'> {
  id: string
  onChange?: (value: boolean) => void
  value?: boolean
  textSim?: string
  textNao?: string
  disabled?: boolean
}

const CustomRadioGroupBoolean = ({
  id,
  onChange,
  value,
  textSim,
  textNao,
  disabled = false,
  ...props
}: CustomRadioGroupBooleanProps) => {
  const handleChage = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange?.(e.target.value === 'sim')
  }

  const getValue = () => {
    switch (value) {
      case true:
        return 'sim'
      case false:
        return 'nao'
      default:
        return null
    }
  }

  return (
    <RadioGroup id={id} onChange={handleChage} value={getValue()} {...props}>
      <FormControlLabel
        id={`${id}--form-control-label-yes`}
        control={<Radio id={`${id}--radio-yes`} size={'small'} color={'primary'} />}
        label={textSim || 'Sim'}
        value={'sim'}
        disabled={disabled}
      />
      <FormControlLabel
        id={`${id}--form-control-label-no`}
        control={<Radio id={`${id}--radio-no`} size={'small'} color={'primary'} />}
        label={textNao || 'Não'}
        value={'nao'}
        disabled={disabled}
      />
    </RadioGroup>
  )
}

export default CustomRadioGroupBoolean
