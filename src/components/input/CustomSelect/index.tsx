import React from 'react'
import { CustomTextField, CustomTextFieldProps } from '../CustomTextField'
import { MenuItem } from '@mui/material'

export interface CustomSelectProps extends CustomTextFieldProps {
  options: {
    label: string | React.ReactNode
    value: string
  }[]
}

const CustomSelect = ({ options, ...props }: CustomSelectProps) => {
  return (
    <CustomTextField
      select
      {...props}
      SelectProps={{
        MenuProps: {
          style: {
            maxHeight: 500
          }
        },
        ...props.SelectProps
      }}
    >
      {options.map((op, index) => {
        return (
          <MenuItem id={`${props.id}--menu-item--${index}`} key={op.value} value={op.value}>
            {op.label}
          </MenuItem>
        )
      })}
    </CustomTextField>
  )
}

export default CustomSelect
