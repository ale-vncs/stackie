import {
  alpha,
  debounce,
  InputBase,
  InputBaseProps,
  Paper,
  Slider,
  SliderProps,
  Stack,
  Typography,
  useTheme
} from '@mui/material'
import React, { useCallback } from 'react'

export interface CustomSliderProps extends Omit<SliderProps, 'onChange'> {
  id: string
  onChangeValue: (v: number | number[]) => void
  label?: string
  InputBaseProps?: InputBaseProps
}

const CustomSlider = ({
  id,
  onChangeValue,
  InputBaseProps,
  label,
  max = 100,
  min = 0,
  ...props
}: CustomSliderProps) => {
  const { palette } = useTheme()
  const debounceValue = useCallback(
    debounce((d: number | number[]) => onChangeValue?.(d), 50),
    []
  )

  const onChangeTextField = (text: string) => {
    const value = Number(text.replace(/\D+/g, ''))
    if (value > max) {
      debounceValue(max)
      return
    }

    if (value < min) {
      debounceValue(min)
      return
    }

    debounceValue(value)
  }

  return (
    <Paper
      variant={'outlined'}
      sx={{
        height: 40,
        px: 1.5,
        borderWidth: 1,
        borderColor: alpha(palette.text.primary, 0.23),
        background: palette.background.paper,
        backgroundImage: 'linear-gradient(rgba(255, 255, 255, 0.16), rgba(255, 255, 255, 0.16))',
        boxSizing: 'border-box',
        position: 'relative'
      }}
    >
      <Typography
        component={'label'}
        color={alpha(palette.text.primary, 0.7)}
        sx={{
          position: 'absolute',
          top: -15,
          left: -5,
          background: 'inherit',
          px: 0.5,
          transform: 'scale(0.75)'
        }}
      >
        {label}
      </Typography>
      <Stack spacing={2} direction="row" alignItems="center" height={'100%'}>
        <Slider
          id={`${id}--slider`}
          valueLabelDisplay={'auto'}
          size={'small'}
          {...props}
          max={max}
          min={min}
          onChange={(_, v) => debounceValue(v)}
        />
        <InputBase
          {...InputBaseProps}
          sx={{
            width: 30,
            ...InputBaseProps?.sx
          }}
          id={`${id}--input-base`}
          onChange={(e) => onChangeTextField(e.target.value)}
          value={props.value}
        />
      </Stack>
    </Paper>
  )
}

export default CustomSlider
