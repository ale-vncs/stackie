import React from 'react'
import Switch, { SwitchProps } from '@mui/material/Switch'
import { FormControl, FormControlLabel, FormControlProps, FormGroup, FormLabel, Typography } from '@mui/material'
import { FormControlLabelProps } from '@mui/material/FormControlLabel'

interface CustomSwitchProps {
  id: string
  checked: boolean
  selectedLabel: string
  notSelectedLabel?: string
  title?: string
  onChange?: (value: boolean) => void
  formControlLabel?: Omit<FormControlLabelProps, 'label' | 'control'>
  switchProps?: SwitchProps
  formControl?: FormControlProps
}

export const CustomSwitch = ({
  id,
  selectedLabel,
  notSelectedLabel,
  title,
  switchProps,
  formControlLabel,
  formControl,
  onChange,
  checked
}: CustomSwitchProps) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.checked
    onChange?.(value)
  }

  if (!notSelectedLabel) notSelectedLabel = selectedLabel

  return (
    <FormControl {...formControl}>
      {title && (
        <FormLabel focused={false}>
          <Typography variant={'subtitle2'}>{title}</Typography>
        </FormLabel>
      )}
      <FormGroup>
        <FormControlLabel
          {...formControlLabel}
          control={
            <Switch
              {...switchProps}
              id={`${id}-switch`}
              size={'small'}
              checked={checked}
              onChange={handleChange}
              color={'primary'}
            />
          }
          label={<Typography variant={'subtitle2'}>{checked ? selectedLabel : notSelectedLabel}</Typography>}
        />
      </FormGroup>
    </FormControl>
  )
}

export default CustomSwitch
