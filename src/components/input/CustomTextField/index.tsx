import React, { useCallback, useEffect, useRef, useState } from 'react'
import { debounce, InputAdornment, TextField } from '@mui/material'
import { getNumber, mask } from '@utils/string.util'
import { TextFieldProps } from '@mui/material/TextField'
import { Null, Undefined } from '@typings/generic.typing'
import useFirstRender from '@hooks/useFirstRender'

type Pattern = [boolean, string] | string

export interface CustomTextFieldProps extends Omit<TextFieldProps, 'value' | 'onChange'> {
  id: string
  pattern?: Pattern[] | string
  justNumber?: boolean
  onChangeText?: (text: string) => void
  showLimitCounter?: boolean
  value?: Null<string>
  readOnly?: boolean
  endIcon?: React.ReactNode
  startIcon?: React.ReactNode
  fieldLimit?: number
  debounceTime?: number
}

export const CustomTextField = ({
  id,
  pattern,
  justNumber,
  value,
  onChangeText,
  readOnly = false,
  showLimitCounter = true,
  endIcon,
  startIcon,
  fieldLimit,
  debounceTime = 50,
  ...props
}: CustomTextFieldProps) => {
  const [textValue, setTextValue] = useState('')
  const lastPattern = useRef('')
  const lastValues = useRef(['', ''])
  const { isFirstRender } = useFirstRender()

  const debounceText = useCallback(
    !debounceTime ? (d: string) => onChangeText?.(d) : debounce((d: string) => onChangeText?.(d), debounceTime),
    [onChangeText, debounceTime]
  )

  useEffect(() => {
    if (value) {
      if (!lastValues.current.includes(value) || isFirstRender || pattern) {
        applyMask(value)
      }
    } else {
      setTextValue((props.defaultValue as Undefined<string>) || '')
    }
  }, [value])

  const limitarTexto = (text: string) => {
    if (fieldLimit && text.length > fieldLimit) {
      return text.slice(0, fieldLimit)
    }
    return text
  }

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value
    applyMask(value)
  }
  const applyMask = (value: string) => {
    let val: string
    if (pattern) {
      let patt = ''
      if (typeof pattern === 'string') {
        patt = pattern
      } else {
        for (const pt of pattern) {
          if (typeof pt[0] === 'boolean') {
            patt = pt[1]
            break
          } else if (typeof pt === 'string') {
            const lenPattern = (pt.match(/#/g) || []).length
            if (lenPattern >= getNumber(value).length) {
              patt = pt
              break
            }
          }
        }
      }

      patt ? (lastPattern.current = patt) : (patt = lastPattern.current)
      value = value.substring(0, patt.length)
      const unMask = getNumber(value)

      const maskValue = mask(unMask, patt, justNumber)

      setTextValue(limitarTexto(maskValue))
      val = unMask
      lastValues.current = [val, value]
    } else {
      val = justNumber ? getNumber(value) : value
      setTextValue(limitarTexto(val))
    }

    if (onChangeText) debounceText(limitarTexto(val))
  }

  const getLimitCaracterText = () => {
    if (!fieldLimit) return undefined
    return `${fieldLimit - textValue.length} caracter(es) restantes`
  }

  const getEndIcon = () => {
    if (!endIcon) return undefined
    return <InputAdornment position={'end'}>{endIcon}</InputAdornment>
  }

  const getStartIcon = () => {
    if (!startIcon) return undefined
    return <InputAdornment position={'start'}>{startIcon}</InputAdornment>
  }

  const getHelperText = () => {
    if (props) {
      if (props.helperText) {
        return props.helperText
      }
      if (showLimitCounter) {
        return getLimitCaracterText()
      }
    }
    return undefined
  }

  return (
    <TextField
      id={id}
      size={'small'}
      variant={'outlined'}
      {...props}
      helperText={getHelperText()}
      InputProps={{
        endAdornment: getEndIcon(),
        startAdornment: getStartIcon(),
        ...props.InputProps
      }}
      inputProps={{
        readOnly,
        style: readOnly ? { cursor: 'default' } : undefined,
        autoComplete: 'off',
        maxLength: fieldLimit,
        ...props.inputProps
      }}
      onChange={onChange}
      value={textValue}
    />
  )
}

export default CustomTextField
