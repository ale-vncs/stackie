import { useRunnerObserver } from '@hooks/useRunnerObserver'
import { RunnerStatus } from '@slices/runner.slice'

export const useCurrentRunnerSelected = () => {
  const runners = useRunnerObserver((state) => state.runners)
  const runnerIdSelected = useRunnerObserver((state) => state.runnerIdSelected)

  const getCurrentRunner = () => {
    const runnerIdAvailable = runnerIdSelected.filter((r) => !Object.values<string>(RunnerStatus).includes(r))
    if (runnerIdAvailable.length > 1) return null

    const runner = runners.find((r) => r.config.runnerId === runnerIdAvailable[0])
    if (runner) return runner
    return null
  }

  return {
    runner: getCurrentRunner()
  }
}
