import { useAppDispatch, useAppSelector } from '@hooks/useRedux'
import { useStorage } from '@hooks/useStorage'
import { setProjectList } from '@slices/storage.slice'
import { useEffect } from 'react'

export const useProject = () => {
  const { getData } = useStorage()
  const dispatch = useAppDispatch()
  const projects = useAppSelector((store) => store.storage.projects)

  useEffect(() => {
    loadProject()
  }, [])

  const loadProject = () => {
    getProjectList().then((p) => dispatch(setProjectList(p)))
  }

  const getProjectList = () => {
    return getData('projectList')
  }

  const reloadAllProject = () => {
    window.electron.loadProject().then(() => {
      loadProject()
    })
  }

  return {
    projects,
    loadProject,
    getProjectList,
    reloadAllProject
  }
}
