import { useAppDispatch, useAppSelector } from '@hooks/useRedux'
import { useEffect } from 'react'
import { useStorage } from '@hooks/useStorage'
import { setRunConfiguration as setRunConfigurationSlice } from '@slices/storage.slice'
import { RunConfiguration } from '@typings/runConfiguration.typing'

export const useRunConfiguration = () => {
  const { getData, setData } = useStorage()
  const dispatch = useAppDispatch()
  const runConfigurationList = useAppSelector((state) => state.storage.runConfigurations)

  useEffect(() => {
    getData('runConfigurationList').then((runs) => {
      setRunConfiguration(runs)
    })
  }, [])

  const setRunConfiguration = (runners: RunConfiguration[]) => {
    dispatch(setRunConfigurationSlice(runners))
    setData('runConfigurationList', runners)
  }

  return {
    runConfigurationList,
    setRunConfiguration
  }
}
