import { useAppDispatch } from '@hooks/useRedux'
import { RunConfiguration } from '@typings/runConfiguration.typing'
import { setRunnerIdSelected as setRunnerIdSelectedSlice, setRunnersFromRunConfiguration } from '@slices/runner.slice'

export const useRunner = () => {
  const dispatch = useAppDispatch()

  const setFromRunConfiguration = (runConfiguration: RunConfiguration[]) => {
    dispatch(setRunnersFromRunConfiguration(runConfiguration))
  }

  const setRunnerIdSelected = (runnerIdList: string[]) => {
    dispatch(setRunnerIdSelectedSlice(runnerIdList))
  }

  return {
    setFromRunConfiguration,
    setRunnerIdSelected
  }
}
