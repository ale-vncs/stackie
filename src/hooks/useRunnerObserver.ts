import { useAppSelector } from '@hooks/useRedux'
import { RunnerState } from '@slices/runner.slice'

export const useRunnerObserver = <T>(fn: (state: RunnerState) => T) => {
  return useAppSelector((state) => fn(state.runner))
}
