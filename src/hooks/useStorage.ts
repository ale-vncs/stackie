import { StorageData, StorageDataKey, StorageSetDataReturn } from '@typings/storageData.typing'

export const useStorage = () => {
  const setData = async <K extends StorageDataKey>(key: K, fn: StorageSetDataReturn<K>) => {
    await window.electron.saveData(key, fn)
  }

  const getData = async <K extends StorageDataKey>(key: K): Promise<StorageData[K]> => {
    return await window.electron.getData(key)
  }

  return {
    setData,
    getData
  }
}
