import {
  IpcAsyncFunction,
  IpcMainToRenderer,
  IpcRendererGenericChannel,
  IpcRendererToMain
} from '@typings/ipcElectron.typing'
import { contextBridge, ipcRenderer } from 'electron'

export class IpcRendererImpl {
  static init() {
    const ipcFunctionArray: (keyof IpcAsyncFunction)[] = [
      'saveData',
      'getData',
      'existFile',
      'getOperationalSystem',
      'cloneGitProject',
      'loadProject',
      'getScriptPackageJson',
      'loadDevkitsVersion',
      'downloadDevkit'
    ]
    const ipcFunctionToMain: (keyof IpcRendererToMain)[] = ['spawn']
    const ipcFunctionFromMain: (keyof IpcMainToRenderer)[] = []

    const ipcRendererContext: Record<string, unknown> = {}

    ipcFunctionArray.forEach((key) => {
      ipcRendererContext[key] = async (...args: unknown[]) => {
        return await ipcRenderer.invoke(key as string, ...args)
      }
    })

    ipcFunctionToMain.forEach((key) => {
      ipcRendererContext[key] = (...args: unknown[]) => {
        return ipcRenderer.send(key as string, ...args)
      }
    })

    ipcFunctionFromMain.forEach((key) => {
      ipcRendererContext[key] = (cb: (...args: unknown[]) => void) =>
        ipcRenderer.on(key as string, (_, ...args) => cb(...args))
    })

    const ipcRendererGeneric: IpcRendererGenericChannel = {
      spawnListener: (channel, fn) => {
        const spawnChannel = `spawn-${channel}`
        const spawnChannelClose = `spawn-${channel}-close`
        ipcRenderer.on(spawnChannel, (_, args) => {
          fn.output?.(args)
        })
        ipcRenderer.on(spawnChannelClose, (_, [code, err]) => {
          fn.onClose?.(code, err)
        })
      }
    }

    Object.assign(ipcRendererContext, ipcRendererGeneric)

    contextBridge.exposeInMainWorld('electron', ipcRendererContext)
  }

  static send<K extends keyof IpcRendererToMain>(channel: K, ...data: Parameters<IpcRendererToMain[K]>) {
    ipcRenderer.send(channel as string, ...data)
  }
}
