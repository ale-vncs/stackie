import { IpcAsyncFunction, IpcMainToRenderer, IpcRendererToMain } from '@typings/ipcElectron.typing'
import { ipcMain } from 'electron'

export class IpcMainImpl {
  static registerAsync<K extends keyof IpcAsyncFunction>(key: K, fn: IpcAsyncFunction[K]) {
    ipcMain.handle(key as string, async (event, ...args) => {
      console.log({ key, args })
      return await fn(...args)
    })
  }

  static register<K extends keyof IpcRendererToMain>(
    channel: K,
    cb: (ev: Electron.IpcMainEvent) => IpcRendererToMain[K]
  ) {
    ipcMain.on(channel as string, (event, ...args: unknown[]) => {
      console.log({ key: channel, args })
      cb(event)(...args)
    })
  }

  static send<K extends keyof IpcMainToRenderer>(
    wb: Electron.WebContents,
    channel: K,
    ...data: Parameters<IpcMainToRenderer[K]>
  ) {
    wb.send(channel as string, ...data)
  }
}
