import { IpcMainImpl } from '../ipcMainImpl'
import { download } from 'electron-dl'
import decompress from 'decompress'
import { BrowserWindow } from 'electron'
import os from 'os'
import { pathUtil } from '@utils/path.util'
import path from 'path'
import { AppStore } from '../../appStore'
import { Devkit } from '@typings/project.typing'

const { nodeVersionPath, tempPath } = pathUtil()

IpcMainImpl.registerAsync('downloadDevkit', async (devkit, version) => {
  if (devkit === 'node') {
    return await downloadNode(version)
  }

  return false
})

const downloadNode = async (version: string) => {
  // https://nodejs.org/dist/v18.16.0/node-v18.16.0-win-x64.zip

  const platform = os.platform()
  const arc = os.arch()
  console.log({ platform, arc })
  let platformName = ''
  let arcName = ''
  let extension = ''

  if (platform === 'win32') {
    platformName = 'win'
    arcName = arc
    extension = 'zip'
  } else if (platform === 'linux') {
    platformName = 'linux'
    arcName = arc
    extension = 'tar.gz'
  } else if (platform === 'darwin') {
    platformName = 'darwin'
    arcName = arc
    extension = 'tar.gz'
  }

  const url = `https://nodejs.org/dist/${version}/node-${version}-${platformName}-${arcName}.${extension}`

  const r = await downloadFile(url)

  const isCompleted = r.getState() === 'completed'

  if (!isCompleted) return false

  await unzip(r.getFilename(), 'node', version)

  AppStore.setStoreData('managerVersion', (d) => {
    d.npm.downloaded.push(version)

    return d
  })

  return isCompleted
}

const unzip = async (filename: string, devkit: Devkit, version: string) => {
  let dist = ''
  let strip = 0

  if (devkit === 'node') {
    dist = path.join(nodeVersionPath, version)
    strip = 1
  }

  if (!dist) {
    throw new Error(`${devkit} não configurado unzip `)
  }

  await decompress(path.join(tempPath, filename), dist, {
    strip
  })
}

const downloadFile = async (url: string) => {
  const focusedWindow = BrowserWindow.getFocusedWindow()
  if (!focusedWindow) throw new Error('Sem BrowserWindow')

  return await download(focusedWindow, url, {
    directory: tempPath
  })
}
