import { IpcMainImpl } from '../ipcMainImpl'
import { createPath, pathUtil } from '@utils/path.util'
import { spawnSync } from 'child_process'
import path from 'path'
import { loadAllProject } from './loadProject.register'

const getDataGit = (url: string) => {
  const name = url.match(/([^/]+)(?=\.git)/)
  const domain = url.match(/^(?:https?:\/\/)?(?:[^@/\n]+@)?(?:www\.)?([^:/\n]+)/)

  if (!name || !domain) {
    throw new Error(`Error get project name or domain from git: ${url}`)
  }

  return {
    domain: domain[1],
    name: name[1]
  }
}

const createPathWithDomain = (domain: string) => {
  const { domainPath } = pathUtil()
  const folderDomainPath = path.join(domainPath, domain)
  createPath(folderDomainPath)

  return {
    folderDomainPath
  }
}

IpcMainImpl.registerAsync('cloneGitProject', async (url) => {
  const { domain } = getDataGit(url)
  const { folderDomainPath } = createPathWithDomain(domain)

  const stats = spawnSync('git', ['-C', folderDomainPath, 'clone', url], {
    encoding: 'utf8'
  })

  if (stats.status === 0) {
    loadAllProject()
  }

  return stats
})
