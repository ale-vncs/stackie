import './spawn.register'
import './gitClone.register'
import './loadProject.register'
import './downloadDevkit.register'
import './loadDevkitsVersion.register'
import { AppStore } from '../../appStore'
import os from 'os'
import { IpcMainImpl } from '../ipcMainImpl'
import fs from 'fs'
import path from 'path'

IpcMainImpl.registerAsync('getData', async (key) => {
  return AppStore.getStoreData(key)
})

IpcMainImpl.registerAsync('saveData', async (key, data) => {
  AppStore.setStoreData(key, data)
})

IpcMainImpl.registerAsync('getOperationalSystem', async () => {
  const so = os.platform()
  if (so === 'win32') return 'windows'
  if (so === 'linux') return 'linux'
  if (so === 'darwin') return 'mac'
  throw new Error('Incorrect System')
})

IpcMainImpl.registerAsync('existFile', async (path) => {
  return fs.existsSync(path)
})

IpcMainImpl.registerAsync('getScriptPackageJson', async (projectName) => {
  const projects = AppStore.getStoreData('projectList')

  const project = projects.find((p) => p.name === projectName)

  if (project) {
    const packageJson = fs.readFileSync(path.join(project.path, 'package.json'), { encoding: 'utf8' })
    const json = JSON.parse(packageJson)

    return Object.keys(json.scripts)
  }

  return []
})
