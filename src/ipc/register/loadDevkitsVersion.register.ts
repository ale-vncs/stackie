import { IpcMainImpl } from '../ipcMainImpl'
import { AppStore } from '../../appStore'
import { pathUtil } from '@utils/path.util'
import { NpmManagerVersionData } from '@typings/managerVersion.typing'
import fs from 'fs'

IpcMainImpl.registerAsync('loadDevkitsVersion', async () => {
  AppStore.resetData('managerVersion')
  const { nodeVersionPath } = pathUtil()

  const promises = []

  const npmVersion = fetch('https://nodejs.org/dist/index.json', {
    method: 'GET'
  })
    .then((res) => {
      if (res.ok) {
        return res.json() as Promise<NpmManagerVersionData[]>
      }
      return []
    })
    .then((version) => {
      const versionNode = version.map((v) => v.version)
      const versionDownloaded = fs.readdirSync(nodeVersionPath).filter((v) => versionNode.includes(v))

      AppStore.setStoreData('managerVersion', (d) => {
        d.npm.versionDataList = version
        d.npm.available = versionNode.filter((v) => !versionDownloaded.includes(v))
        d.npm.downloaded = versionDownloaded
        return d
      })
    })

  promises.push(npmVersion)

  await Promise.all(promises)
})
