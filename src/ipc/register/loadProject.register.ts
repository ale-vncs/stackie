import { IpcMainImpl } from '../ipcMainImpl'
import { pathUtil } from '@utils/path.util'
import fs from 'fs'
import path from 'path'
import { AppStore } from '../../appStore'
import { ProjectData, ProjectManager } from '@typings/project.typing'
import { spawnSync } from 'child_process'
import { orderArrayBy } from '@utils/array.util'

const getGitUrl = (projectPath: string) => {
  const child = spawnSync('git', ['-C', projectPath, 'remote', 'get-url', 'origin'], {
    encoding: 'utf8'
  })

  if (child.status !== 0) {
    throw new Error('Git url dont found')
  }

  return child.stdout.trim()
}

const getLabelProject = (projectList: ProjectData[], domain: string, projectName: string) => {
  const project = projectList.find((p) => p.name === projectName && p.domain === domain)
  if (project) return project.label
  return projectName
}

const getProjectManager = (projectPath: string) => {
  const files = fs.readdirSync(projectPath)

  const parseFileToManager = {
    'package.json': ProjectManager.NPM,
    'pom.xml': ProjectManager.MAVEN
  }

  for (const [file, manager] of Object.entries(parseFileToManager)) {
    if (files.includes(file)) return manager
  }

  return ProjectManager.UNKNOWN
}

export const loadAllProject = () => {
  const { domainPath } = pathUtil()
  const projectOldList = AppStore.getStoreData('projectList')
  const domains = fs.readdirSync(domainPath)

  const newProjectList: ProjectData[] = domains
    .map((d) => {
      const projectsByDomain = fs.readdirSync(path.join(domainPath, d))
      return projectsByDomain.map((p) => ({
        domain: d,
        projectName: p
      }))
    })
    .flat()
    .map(({ domain, projectName }): ProjectData => {
      const projectPath = path.join(domainPath, domain, projectName)
      const gitUrl = getGitUrl(projectPath)
      const label = getLabelProject(projectOldList, domain, projectName)

      return {
        label,
        domain,
        gitUrl,
        name: projectName,
        path: projectPath,
        manager: getProjectManager(projectPath)
      }
    })

  orderArrayBy(newProjectList, ['domain', 'name'], ['asc', 'asc'])
  AppStore.setStoreData('projectList', () => newProjectList)

  return newProjectList
}

IpcMainImpl.registerAsync('loadProject', async () => {
  return loadAllProject()
})
