import { spawn } from 'child_process'
import { IpcMainImpl } from '../ipcMainImpl'

IpcMainImpl.register('spawn', (ev) => (channel, config) => {
  const { command, args, options } = config
  const child = spawn(command, args, { ...options })

  const spawnChannel = `spawn-${channel}`
  const spawnChannelClose = `spawn-${channel}-close`

  const send = (data: unknown) => {
    ev.sender.send(spawnChannel, channel, data)
  }

  const close = (code: number, err?: unknown) => {
    ev.sender.send(spawnChannelClose, channel, err)
  }

  if (child.stdout) {
    child.stdout.on('data', (data) => {
      send(data)
    })
  }

  if (child.stderr) {
    child.stderr.on('data', (data) => {
      send(data)
    })
  }

  child.on('error', (err) => {
    close(-1, err)
  })

  child.on('close', (code) => {
    close(code ?? -1)
  })
})
