import './app.css'
import { CustomThemeProvider } from '@providers/CustomThemeProvider'
import { HashRouter, Route, RouteProps, Routes, useLocation, useOutlet } from 'react-router-dom'
import { CSSTransition, SwitchTransition } from 'react-transition-group'
import { ReduxProvider } from '@providers/ReduxProvider'
import { Home } from './Home'
import { CheckSystem } from './CheckSystem'
import { GeneralConfig } from './GeneralConfig'
import { createRef, memo, RefObject, useMemo } from 'react'
import { ProjectManager } from './Home/ProjectManager'
import { RunConfiguration } from './Home/RunConfiguration'

type SystemRoute = RouteProps & { nodeRef: RefObject<HTMLDivElement> }

export const App = () => {
  const routes: RouteProps[] = [
    {
      path: '/',
      element: <CheckSystem />
    },
    {
      path: '/home',
      element: <Home />
    },

    {
      path: '/config',
      element: <GeneralConfig />
    },
    {
      path: '/project-manager',
      element: <ProjectManager />
    },
    {
      path: '/runner-config',
      element: <RunConfiguration />
    }
  ]

  const systemRoute = useMemo(() => {
    const r: SystemRoute[] = routes.map((r) => {
      return { ...r, nodeRef: createRef() }
    })
    return r
  }, [])

  return (
    <ReduxProvider>
      <CustomThemeProvider>
        <HashRouter>
          <Routes>
            <Route path={'/'} element={<AnimationElementMemo routes={systemRoute} />}>
              {routes.map((r) => (
                <Route key={r.path} path={r.path} element={r.element} index={r.path === '/main_window'} />
              ))}
            </Route>
          </Routes>
        </HashRouter>
      </CustomThemeProvider>
    </ReduxProvider>
  )
}

const AnimationElement = ({ routes }: { routes: SystemRoute[] }) => {
  const location = useLocation()
  const outlet = useOutlet()
  const { nodeRef } = routes.find((route) => route.path === location.pathname) ?? {}

  return (
    <SwitchTransition>
      <CSSTransition key={location.pathname} nodeRef={nodeRef} timeout={300} classNames="page" unmountOnExit>
        <div ref={nodeRef} className="page">
          {outlet}
        </div>
      </CSSTransition>
    </SwitchTransition>
  )
}

const AnimationElementMemo = memo(AnimationElement)
