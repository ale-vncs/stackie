import { CircularProgress, Grid, Stack, Typography, useTheme } from '@mui/material'
import { useEffect, useState } from 'react'
import { BsCheckCircle, BsExclamationCircle, BsPauseCircle, BsXCircle } from 'react-icons/bs'
import { useNavigate } from 'react-router-dom'
import { useStorage } from '@hooks/useStorage'

interface CheckSystemDependencies {
  name: string
  msg: string
  promise: () => Promise<unknown>
  status?: 'success' | 'error' | 'checking' | 'warning'
  required: boolean
}

export const CheckSystem = () => {
  const { setData, getData } = useStorage()
  const navigate = useNavigate()

  const [systemMsg, setSystemMsg] = useState('Verificando dependências')
  const [systemDependencies, setSystemDependencies] = useState<CheckSystemDependencies[]>([])

  const checkGitPath = async () => {
    const gitPathBySystem = (so: string) => {
      const gitPath: Record<string, string> = {
        windows: 'C:\\Program Files\\Git\\bin\\git.exe'
      }

      if (!(so in gitPath)) {
        throw new Error(`git path não configurado no ${so}`)
      }

      return gitPath[so]
    }
    const currentGitPath = await getData('gitPath')

    if (currentGitPath) return window.electron.existFile(currentGitPath)

    const so = await window.electron.getOperationalSystem()
    const gitPath = gitPathBySystem(so)
    const existGit = await window.electron.existFile(gitPath)

    if (existGit) setData('gitPath', gitPath)

    return existGit
  }

  const checkProjects = async () => {
    await window.electron.loadProject()
  }

  const updateStatusSystem = (name: string, status: CheckSystemDependencies['status']) => {
    setSystemDependencies((prev) => {
      return prev.map((d) => {
        return {
          ...d,
          status: d.name === name ? status : d.status
        }
      })
    })
  }

  const checkSystem = async (dependencies: CheckSystemDependencies[]) => {
    await window.electron.loadDevkitsVersion()

    for (const dep of dependencies) {
      updateStatusSystem(dep.name, 'checking')
      try {
        await dep.promise()
      } catch (e) {
        console.log(e)
        if (dep.required) {
          updateStatusSystem(dep.name, 'error')
          setSystemMsg(`Error ao carregar ${dep.name}`)
          break
        }
        updateStatusSystem(dep.name, 'warning')
        continue
      }
      updateStatusSystem(dep.name, 'success')
    }
  }

  useEffect(() => {
    const dependencies: CheckSystemDependencies[] = [
      {
        name: 'git',
        msg: 'Verificando git',
        promise: checkGitPath,
        required: false
      },
      {
        name: 'load-project',
        msg: 'Carregando projetos',
        promise: checkProjects,
        required: false
      }
    ]
    setSystemDependencies(dependencies)
    checkSystem(dependencies)
  }, [])

  useEffect(() => {
    if (!systemDependencies.length) return
    let success = 0
    systemDependencies.forEach((dep) => {
      if (dep.status === 'success') success += 1
      if (dep.status === 'warning') success += 1
    })
    if (success === systemDependencies.length) {
      setSystemMsg('Tudo verificado, iniciando...')
      setTimeout(() => navigate('/home'), 1800)
    }
  }, [systemDependencies])

  return (
    <Grid container direction={'column'} justifyContent={'center'} alignItems={'center'} height={'100%'}>
      <Grid item>
        <Typography>{systemMsg}</Typography>
      </Grid>
      {systemDependencies.map((dep) => (
        <Grid key={dep.name} item>
          <Stack direction={'row'} alignItems={'center'} columnGap={2}>
            <Typography>{dep.msg}</Typography>
            <DepStatus status={dep.status} />
          </Stack>
        </Grid>
      ))}
    </Grid>
  )
}

const DepStatus = ({ status }: { status?: CheckSystemDependencies['status'] }) => {
  const { palette } = useTheme()

  if (!status) {
    return <BsPauseCircle color={palette.info.main} />
  }

  if (status === 'success') {
    return <BsCheckCircle color={palette.success.main} />
  }

  if (status === 'error') {
    return <BsXCircle color={palette.error.main} />
  }

  if (status === 'warning') {
    return <BsExclamationCircle color={palette.warning.main} />
  }

  return <CircularProgress size={18} />
}
