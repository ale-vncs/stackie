import { SimpleDialog } from '@components/Dialog/SimpleDialog'
import { useState } from 'react'
import { CustomButton } from '@components/input'
import { Grid } from '@mui/material'
import { FormTextField } from '@components/FormElement'
import { FormGitAccessTokenData, useFormGitAccessToken } from './useFormGitAccessToken'
import { useStorage } from '@hooks/useStorage'

interface GitAccessTokenModalProps {
  onSubmitSuccess: () => void
}

export const GitAccessTokenModal = ({ onSubmitSuccess }: GitAccessTokenModalProps) => {
  const { setData } = useStorage()
  const { control, submit } = useFormGitAccessToken()
  const [openModal, setOpenModal] = useState(false)

  const onSubmit = async (data: FormGitAccessTokenData) => {
    await setData('gitAccessTokenList', (accessToken) => {
      accessToken.push(data)
      return accessToken
    })
    setOpenModal(false)
    onSubmitSuccess()
  }

  return (
    <>
      <CustomButton
        id={'a4544334-c690-4249-9203-6b690c241dfc'}
        variant={'contained'}
        onClick={() => setOpenModal(true)}
      >
        Adicionar Access Token
      </CustomButton>
      <SimpleDialog
        open={openModal}
        onClose={() => setOpenModal(false)}
        title={'Adicionar Access Token'}
        maxWidth={'sm'}
        fullWidth
        confirmLabel={'Adicionar'}
        confirmBtnProps={{
          form: '0e74410d-5433-47c3-956d-aab22ac52e82',
          type: 'submit'
        }}
      >
        <Grid
          container
          direction={'column'}
          rowGap={2}
          component={'form'}
          id={'0e74410d-5433-47c3-956d-aab22ac52e82'}
          onSubmit={submit(onSubmit)}
        >
          <Grid item>
            <FormTextField
              id={'282fe37a-6bfb-4dfb-8dc6-ea2eec66099b'}
              control={control}
              name={'domain'}
              label={'Domínio'}
              fullWidth
            />
          </Grid>
          <Grid item>
            <FormTextField
              id={'cac544ba-8f2f-4036-8466-de6d5c0a65ea'}
              control={control}
              name={'accessToken'}
              label={'Token'}
              fullWidth
            />
          </Grid>
        </Grid>
      </SimpleDialog>
    </>
  )
}
