import { Grid, Paper, Stack, Typography } from '@mui/material'
import { useEffect, useState } from 'react'
import { useStorage } from '@hooks/useStorage'
import { GitAccessToken } from '@typings/storageData.typing'
import { BoxFrame } from '@components/index'
import { CustomIconButton } from '@components/input'
import { FaTrash } from 'react-icons/fa'
import { GitAccessTokenModal } from './GitAccessTokenModal'

export const GitAccessTokenBox = () => {
  const { getData } = useStorage()

  const [gitAccessTokenList, setGitAccessTokenList] = useState<GitAccessToken[]>([])

  const updateGitAccessTokenList = () => {
    getData('gitAccessTokenList').then((gitAccessTokenList) => {
      if (gitAccessTokenList) setGitAccessTokenList(gitAccessTokenList)
    })
  }

  useEffect(() => {
    updateGitAccessTokenList()
  }, [])

  return (
    <BoxFrame label={'Git Access Token'}>
      <Grid container spacing={2} justifyContent={'center'}>
        {gitAccessTokenList.length === 0 && (
          <Grid item xs={12}>
            <Typography textAlign={'center'}>Sem Tokens</Typography>
          </Grid>
        )}
        {gitAccessTokenList.map((git, i) => (
          <Grid key={git.domain} item>
            <Paper variant={'outlined'} sx={{ p: 2 }}>
              <Stack direction={'row'} columnGap={2} alignItems={'center'}>
                <Typography>{git.domain}</Typography>
                <CustomIconButton id={`icon-button-${i}`} icon={FaTrash} />
              </Stack>
            </Paper>
          </Grid>
        ))}
        <Grid item xs={12} display={'flex'} justifyContent={'center'}>
          <GitAccessTokenModal onSubmitSuccess={updateGitAccessTokenList} />
        </Grid>
      </Grid>
    </BoxFrame>
  )
}
