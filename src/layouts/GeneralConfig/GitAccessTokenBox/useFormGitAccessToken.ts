import { useFormElement, z } from '@components/FormElement'

export interface FormGitAccessTokenData {
  domain: string
  accessToken: string
}

export const useFormGitAccessToken = () => {
  return useFormElement<FormGitAccessTokenData>({
    defaultValues: {
      domain: '',
      accessToken: ''
    },
    validation: z.object({
      domain: z.string().url('Informe um domínio válido'),
      accessToken: z.string().min(1, 'Informe o token')
    })
  })
}
