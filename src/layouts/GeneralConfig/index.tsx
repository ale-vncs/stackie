import { Container, Grid } from '@mui/material'
import { GitAccessTokenBox } from './GitAccessTokenBox'
import { BackFrameScreen } from '@components/FrameScreen'
import { useNavigate } from 'react-router-dom'

export const GeneralConfig = () => {
  const navigate = useNavigate()

  const onClickVoltar = () => {
    navigate('/home')
  }

  // ssh-keyscan [hostname] >> ~/.ssh/known_hosts -> criar no known_host
  // ssh-keygen -F [hostname] -> saber se existe
  // ssh-keygen -R [hostname] -> remover de known_host
  // ssh-keygen -t rsa -b 2048 -C "joe@foobar.com" -N "keypassword" -m PEM -f ./foo_rsa

  // (https://gitlab.com/profile/personal_access_tokens)
  return (
    <BackFrameScreen title={'Configuração Geral'} onClickBack={onClickVoltar}>
      <Container>
        <Grid container direction={'column'} rowGap={3}>
          <Grid item>
            <GitAccessTokenBox />
          </Grid>
        </Grid>
      </Container>
    </BackFrameScreen>
  )
}
