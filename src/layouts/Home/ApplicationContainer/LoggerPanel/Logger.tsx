import { Box } from '@mui/material'

export const Logger = () => {
  return (
    <Box sx={{ flex: 1, overflow: 'auto', width: '100%', backgroundColor: 'background.default' }}>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lacus nunc. Pellentesque habitant morbi tristique
      senectus et netus et malesuada fames ac turpis egestas. Curabitur volutpat metus semper leo pretium imperdiet.
      Donec accumsan purus vulputate, bibendum diam id, suscipit ante. Suspendisse sed nulla a lorem fringilla elementum
      nec eu magna. Pellentesque vulputate risus nec dui euismod ornare. Suspendisse potenti. Nullam vitae euismod
      justo. Ut pulvinar commodo eros, mollis mollis eros auctor eget. Phasellus volutpat mattis elementum. Vestibulum
      et leo convallis, fermentum arcu et, vestibulum est. Class aptent taciti sociosqu ad litora torquent per conubia
      nostra, per inceptos himenaeos. Nunc ac massa nunc. Nullam vestibulum urna vitae ex tincidunt, sit amet aliquet
      nibh venenatis. Donec commodo congue rutrum. Class aptent taciti sociosqu ad litora torquent per conubia nostra,
      per inceptos himenaeos. Mauris ac quam viverra, pretium ex a, gravida metus. Curabitur facilisis condimentum
      luctus. Aenean consectetur ac urna ac iaculis. Praesent lacus lorem, imperdiet in suscipit ut, ullamcorper id
      turpis. Vivamus eget maximus turpis. Mauris sollicitudin augue eu posuere semper. Nulla erat augue, vulputate a
      aliquet eget, convallis vel augue. Vivamus ornare auctor felis, id scelerisque tellus tempus vel. Pellentesque
      quis nisi imperdiet, ullamcorper est a, posuere mauris. In diam tellus, imperdiet et tempus vestibulum, semper ac
      ligula. Cras lacinia porta ligula non rhoncus. Nam sem quam, imperdiet at leo interdum, egestas lacinia felis.
      Curabitur eleifend venenatis urna sit amet tempus. Aliquam finibus purus neque, eu posuere neque porta quis.
      Vivamus in elementum felis. Quisque sollicitudin at tellus dictum feugiat. Morbi ornare ligula nec eros
      vestibulum, vitae hendrerit metus auctor. Nullam hendrerit, ligula quis volutpat malesuada, enim ex sodales
      mauris, ac rhoncus quam purus sed orci. Suspendisse potenti. Morbi viverra purus et pellentesque iaculis. Morbi
      semper mi sit amet metus hendrerit fringilla. Quisque vel tempus ligula, nec rutrum urna. Quisque in ante
      ultrices, euismod quam at, convallis orci. Pellentesque aliquet sed neque id vulputate. Suspendisse vehicula
      hendrerit nibh eu semper. Morbi sit amet lobortis felis. In eu dui non purus vulputate vestibulum sit amet quis
      ante. Fusce ligula nibh, ornare sit amet consequat at, volutpat a ex. Ut ullamcorper velit at leo commodo, quis
      dapibus nisi pharetra. Vivamus vitae tortor tincidunt, semper odio a, eleifend sem. Donec vitae maximus magna, et
      dapibus nulla. Vestibulum mauris tortor, elementum sed euismod eget, fringilla ut diam. Fusce tincidunt aliquet
      fermentum. Mauris et dolor auctor mi bibendum faucibus. Donec a metus feugiat purus facilisis ullamcorper. Quisque
      in risus tortor. Duis ornare auctor dui, a consectetur sem varius non. Quisque sollicitudin feugiat pulvinar.
      Vestibulum ornare, sapien vel aliquam egestas, mauris lorem eleifend augue, sit amet interdum velit tellus sed
      ante. Proin elementum sapien ac lorem porta, ac suscipit mi dictum. Donec nec aliquam neque. Fusce fringilla lacus
      in erat fermentum volutpat nec ut lectus. Phasellus quis pellentesque metus. Cras sit amet ullamcorper lorem, sed
      lacinia leo. Proin egestas ipsum at nisl laoreet, ut placerat sem venenatis. Lorem ipsum dolor sit amet,
      consectetur adipiscing elit. Sed nisi urna, vestibulum nec facilisis vel, tempor nec purus. Aliquam in auctor sem.
      Praesent eleifend, diam in aliquet vulputate, felis massa imperdiet libero, nec efficitur lacus risus vitae velit.
      Aliquam velit purus, congue nec finibus et, malesuada id nibh.
    </Box>
  )
}
