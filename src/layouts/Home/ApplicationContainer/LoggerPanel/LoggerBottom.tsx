import { Button, Stack } from '@mui/material'
import { Runner } from '@slices/runner.slice'

interface LoggerBottomProps {
  runner: Runner
}

export const LoggerBottom = ({ runner }: LoggerBottomProps) => {
  return (
    <Stack direction={'row'} sx={{ px: 1, borderTop: ({ palette }) => `1px solid ${palette.divider}` }}>
      <Button size={'small'} color={'inherit'}>
        master
      </Button>
    </Stack>
  )
}
