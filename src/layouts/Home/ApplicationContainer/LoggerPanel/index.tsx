import { Stack, Typography } from '@mui/material'
import { Logger } from './Logger'
import { LoggerBottom } from './LoggerBottom'
import { useCurrentRunnerSelected } from '@hooks/useCurrentRunnerSelected'

export const LoggerPanel = () => {
  const { runner } = useCurrentRunnerSelected()

  if (!runner)
    return (
      <Stack direction={'column'} justifyContent={'center'} alignItems={'center'} sx={{ flex: 1 }}>
        <Typography>Selecione um runner</Typography>
      </Stack>
    )

  return (
    <Stack direction={'column'} sx={{ flex: 1 }}>
      <Logger />
      <LoggerBottom runner={runner} />
    </Stack>
  )
}
