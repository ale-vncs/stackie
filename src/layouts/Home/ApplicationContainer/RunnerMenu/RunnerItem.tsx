import { Runner } from '@slices/runner.slice'
import TreeItem from '@mui/lab/TreeItem'
import { RunnerItemLabel } from './RunnerItemLabel'

interface RunnerItemProps {
  runner: Runner
}

export const RunnerItem = ({ runner }: RunnerItemProps) => {
  return (
    <TreeItem
      nodeId={runner.config.runnerId}
      label={<RunnerItemLabel status={runner.status} text={runner.config.projectName} />}
    />
  )
}
