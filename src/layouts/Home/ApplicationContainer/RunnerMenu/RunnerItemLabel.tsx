import { RunnerStatus } from '@slices/runner.slice'
import { RunnerStatusIcon } from './RunnerStatusIcon'
import { Stack, Typography } from '@mui/material'

interface TreeItemLabelProps {
  text: string
  status: RunnerStatus
}

export const RunnerItemLabel = ({ status, text }: TreeItemLabelProps) => {
  return (
    <Stack direction={'row'} alignItems={'center'} columnGap={1}>
      <RunnerStatusIcon status={status} />
      <Typography>{text}</Typography>
    </Stack>
  )
}
