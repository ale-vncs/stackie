import { useStorage } from '@hooks/useStorage'
import { useCallback, useEffect } from 'react'
import { FaAngleDown, FaAngleRight } from 'react-icons/fa'
import TreeView from '@mui/lab/TreeView'
import TreeItem from '@mui/lab/TreeItem'
import { useRunner } from '@hooks/useRunner'
import { useRunnerObserver } from '@hooks/useRunnerObserver'
import { Runner, RunnerStatus } from '@slices/runner.slice'
import { RunnerItem } from './RunnerItem'
import { RunnerItemLabel } from './RunnerItemLabel'

export const RunnerMenu = () => {
  const { getData } = useStorage()
  const { setFromRunConfiguration, setRunnerIdSelected } = useRunner()
  const runnerIdSelectedList = useRunnerObserver((state) => state.runnerIdSelected)
  const runners = useRunnerObserver((state) => state.runners)

  const runnerNotStarted = runners.filter((r) => r.status === RunnerStatus.NOT_STARTED)
  const runnerRunning = runners.filter((r) => r.status === RunnerStatus.RUNNING)
  const runnerError = runners.filter((r) => r.status === RunnerStatus.ERROR)
  const runnerFinish = runners.filter((r) => r.status === RunnerStatus.FINISH)

  const getExpanded = useCallback(() => {
    const ex: string[] = []

    const checkExpandedRunner = (list: Runner[], status: RunnerStatus) => {
      list
        .map((r) => r.config.runnerId)
        .forEach((runnerId) => {
          if (runnerIdSelectedList.includes(runnerId)) ex.push(status)
        })
    }

    checkExpandedRunner(runnerNotStarted, RunnerStatus.NOT_STARTED)
    checkExpandedRunner(runnerRunning, RunnerStatus.RUNNING)
    checkExpandedRunner(runnerFinish, RunnerStatus.FINISH)
    checkExpandedRunner(runnerError, RunnerStatus.ERROR)

    return ex
  }, [runners])

  useEffect(() => {
    getData('runConfigurationList').then((data) => {
      setFromRunConfiguration(data)
    })
  }, [])

  return (
    <TreeView
      defaultCollapseIcon={<FaAngleDown />}
      defaultExpandIcon={<FaAngleRight />}
      multiSelect
      selected={runnerIdSelectedList}
      defaultExpanded={getExpanded()}
      onNodeSelect={(_, n) => setRunnerIdSelected(n)}
      sx={{ height: '100%', flexGrow: 1, width: 300, overflowY: 'auto' }}
    >
      <TreeItem
        nodeId={RunnerStatus.RUNNING}
        label={<RunnerItemLabel status={RunnerStatus.RUNNING} text={'Executando'} />}
        hidden={!runnerRunning.length}
      >
        {runnerRunning.map((r) => (
          <RunnerItem key={r.config.runnerId} runner={r} />
        ))}
      </TreeItem>
      <TreeItem
        nodeId={RunnerStatus.FINISH}
        label={<RunnerItemLabel status={RunnerStatus.FINISH} text={'Finalizado'} />}
        hidden={!runnerFinish.length}
      >
        {runnerFinish.map((r) => (
          <RunnerItem key={r.config.runnerId} runner={r} />
        ))}
      </TreeItem>
      <TreeItem
        nodeId={RunnerStatus.ERROR}
        label={<RunnerItemLabel status={RunnerStatus.ERROR} text={'Erro'} />}
        hidden={!runnerError.length}
      >
        {runnerError.map((r) => (
          <RunnerItem key={r.config.runnerId} runner={r} />
        ))}
      </TreeItem>
      <TreeItem
        nodeId={RunnerStatus.NOT_STARTED}
        label={<RunnerItemLabel status={RunnerStatus.NOT_STARTED} text={'Não iniciado'} />}
        hidden={!runnerNotStarted.length}
      >
        {runnerNotStarted.map((r) => (
          <RunnerItem key={r.config.runnerId} runner={r} />
        ))}
      </TreeItem>
    </TreeView>
  )
}
