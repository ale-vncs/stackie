import { RunnerStatus } from '@slices/runner.slice'
import { FaPlay, FaRegDotCircle, FaStop, FaTimes } from 'react-icons/fa'

interface RunnerStatusIconProps {
  status: RunnerStatus
}

export const RunnerStatusIcon = ({ status }: RunnerStatusIconProps) => {
  if (status === RunnerStatus.RUNNING) {
    return <FaPlay />
  }

  if (status === RunnerStatus.FINISH) {
    return <FaStop />
  }

  if (status === RunnerStatus.ERROR) {
    return <FaTimes />
  }

  return <FaRegDotCircle />
}
