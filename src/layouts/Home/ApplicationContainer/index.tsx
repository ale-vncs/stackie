import { Grid } from '@mui/material'
import { RunnerMenu } from './RunnerMenu/RunnerMenu'
import { LoggerPanel } from './LoggerPanel'

export function ApplicationContainer() {
  return (
    <Grid container direction={'row'} wrap={'nowrap'} flex={1}>
      <Grid item sx={{ height: '100%', backgroundColor: 'background.paper', px: 1, pt: 1, flexShrink: 0 }}>
        <RunnerMenu />
      </Grid>
      <Grid item sx={{ display: 'flex', width: '100%', backgroundColor: 'background.default' }}>
        <LoggerPanel />
      </Grid>
    </Grid>
  )
}
