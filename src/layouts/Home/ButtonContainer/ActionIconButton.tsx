import { CustomIconButton } from '@components/input'
import { IconType } from 'react-icons'
import { Grid } from '@mui/material'
import { useRunnerObserver } from '@hooks/useRunnerObserver'
import { useAppStore } from '@hooks/useRedux'
import { Runner } from '@slices/runner.slice'
import { useMemo } from 'react'

interface ActionIconButtonProps {
  id: string
  color: string
  icon: IconType
  hidden?: (runnerIdSelectedList: string[], runConfigurationList: Runner[]) => boolean
}

export const ActionIconButton = ({ icon, id, color, hidden }: ActionIconButtonProps) => {
  const store = useAppStore()
  const btnSize = 15

  const runnerIdSelectedList = useRunnerObserver((state) => state.runnerIdSelected)

  const runConfigList = useMemo(() => {
    return store().runner.runners.filter((r) => runnerIdSelectedList.includes(r.config.runnerId))
  }, [runnerIdSelectedList])

  if (hidden?.(runnerIdSelectedList, runConfigList)) return null

  const disabled = !runnerIdSelectedList.length

  return (
    <Grid item>
      <CustomIconButton
        id={id}
        icon={icon}
        disabled={disabled}
        iconProps={{
          size: btnSize,
          color: disabled ? 'grey' : color
        }}
      />
    </Grid>
  )
}
