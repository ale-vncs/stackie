import { ActionIconButton } from './ActionIconButton'
import { Runner, RunnerStatus } from '@slices/runner.slice'
import { FaPlay } from 'react-icons/fa'
import { useTheme } from '@mui/material'

export const PlayActionIconButton = () => {
  const { palette } = useTheme()

  const hiddenPlayBtn = (runnerIdSelectedList: string[], runConfigList: Runner[]) => {
    const hidden: boolean[] = []

    hidden.push(runnerIdSelectedList.includes(RunnerStatus.RUNNING))
    hidden.push(runConfigList.some((e) => e.status === RunnerStatus.RUNNING))

    return hidden.some(Boolean)
  }

  return (
    <ActionIconButton
      id={'c8ef1953-bb2c-4f71-897e-600b71a2c709'}
      icon={FaPlay}
      color={palette.success.main}
      hidden={hiddenPlayBtn}
    />
  )
}
