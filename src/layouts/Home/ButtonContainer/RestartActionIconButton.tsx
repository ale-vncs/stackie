import { FaUndoAlt } from 'react-icons/fa'
import { ActionIconButton } from './ActionIconButton'
import { useTheme } from '@mui/material'
import { Runner, RunnerStatus } from '@slices/runner.slice'

export const RestartActionIconButton = () => {
  const { palette } = useTheme()

  const hiddenRestartBtn = (runnerIdSelectedList: string[], runConfigList: Runner[]) => {
    const hidden: boolean[] = []

    hidden.push(!runnerIdSelectedList.includes(RunnerStatus.RUNNING))
    hidden.push(runConfigList.some((e) => e.status !== RunnerStatus.RUNNING))

    return hidden.some(Boolean)
  }

  return (
    <ActionIconButton
      id={'c8ef1953-bb2c-4f71-897e-600b71a2c709'}
      icon={FaUndoAlt}
      color={palette.info.main}
      hidden={hiddenRestartBtn}
    />
  )
}
