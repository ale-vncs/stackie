import { FaStop } from 'react-icons/fa'
import { ActionIconButton } from './ActionIconButton'
import { useTheme } from '@mui/material'
import { Runner, RunnerStatus } from '@slices/runner.slice'

export const StopActionIconButton = () => {
  const { palette } = useTheme()

  const hiddenStopBtn = (runnerIdSelectedList: string[], runConfigurationList: Runner[]) => {
    return !runnerIdSelectedList.includes(RunnerStatus.RUNNING)
  }

  return (
    <ActionIconButton
      id={'83951179-cb24-43f0-8f85-d718064a64c1'}
      icon={FaStop}
      color={palette.error.main}
      hidden={hiddenStopBtn}
    />
  )
}
