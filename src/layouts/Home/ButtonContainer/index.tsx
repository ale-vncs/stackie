import { Divider, Grid, IconButton, useTheme } from '@mui/material'
import { FaAngleDoubleDown, FaCog, FaFileDownload, FaSearch, FaTrash } from 'react-icons/fa'
import { PlayActionIconButton } from './PlayActionIconButton'
import { RestartActionIconButton } from './RestartActionIconButton'
import { StopActionIconButton } from './StopActionIconButton'

export function ButtonContainer() {
  const { palette } = useTheme()

  const btnSize = 15

  return (
    <Grid
      container
      direction={'column'}
      height={'100%'}
      rowGap={1}
      sx={{
        backgroundColor: 'background.paper',
        pt: 1,
        px: 1.5,
        borderRightColor: 'divider',
        borderRightWidth: 1,
        borderRightStyle: 'solid'
      }}
    >
      <PlayActionIconButton />
      <RestartActionIconButton />
      <StopActionIconButton />
      <Grid item>
        <IconButton>
          <FaTrash size={btnSize} color={palette.error.main} />
        </IconButton>
      </Grid>
      <Grid item>
        <IconButton>
          <FaAngleDoubleDown size={btnSize} color={palette.error.main} />
        </IconButton>
      </Grid>
      <Grid item>
        <Divider />
      </Grid>
      <Grid item>
        <IconButton>
          <FaSearch size={btnSize} color={palette.warning.main} />
        </IconButton>
      </Grid>
      <Grid item>
        <IconButton>
          <FaFileDownload size={btnSize} color={palette.warning.main} />
        </IconButton>
      </Grid>
      <Grid item>
        <IconButton>
          <FaCog size={btnSize} color={palette.warning.main} />
        </IconButton>
      </Grid>
    </Grid>
  )
}
