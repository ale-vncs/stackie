import { CustomSelect } from '@components/input'
import { useAppDispatch, useAppSelector } from '@hooks/useRedux'
import { useEffect, useState } from 'react'
import { useStorage } from '@hooks/useStorage'
import { Workspace } from '@typings/workspace.typing'
import { changeWorkspace } from '@slices/systemSlice'

export const WorkspaceSelect = () => {
  const { getData, setData } = useStorage()
  const dispatch = useAppDispatch()
  const currentWorkspace = useAppSelector((state) => state.system.currentWorkspace)

  const [workspaceList, setWorkspaceList] = useState<Workspace[]>([])

  const getWorkspaceList = () => {
    getData('workspaceList').then((list) => {
      if (list) setWorkspaceList(list)
    })
  }

  useEffect(() => {
    getWorkspaceList()
    getData('currentWorkspaceId').then((workspace) => {
      if (workspace) dispatch(changeWorkspace(workspace))
    })
  }, [])

  const onChangeSelect = (option: string) => {
    setData('currentWorkspaceId', option)
    dispatch(changeWorkspace(option))
  }

  const selectOption = workspaceList.map((ws) => ({
    label: ws.name,
    value: ws.id
  }))

  return (
    <CustomSelect
      value={currentWorkspace}
      options={selectOption}
      id={'938ff0a7-5aa3-449c-b41d-c04bfe9c4bab'}
      onChangeText={onChangeSelect}
      label={'Workspace'}
    />
  )
}
