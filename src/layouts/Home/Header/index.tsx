import { Grid } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import { WorkspaceSelect } from './WorkspaceSelect'
import { CustomIconButtonTooltip } from '@components/input'
import { IconBaseProps } from 'react-icons'
import { MdBuild, MdInstallDesktop, MdPlayArrow } from 'react-icons/md'
import { CustomIconButtonTooltipProps } from '@components/input/CustomIconButtonTooltip'

export function Header() {
  const navigate = useNavigate()
  const headerHeightSize = 60

  const handleGeneralConfig = () => {
    navigate('/config')
  }

  const handleProjectManager = () => {
    navigate('/project-manager')
  }

  const handleRunnerConfig = () => {
    navigate('/runner-config')
  }

  const iconProps: IconBaseProps = {
    size: 20
  }

  const tooltipProps: CustomIconButtonTooltipProps['tooltipProps'] = {
    slotProps: {
      tooltip: {
        style: { marginTop: '32px' }
      }
    }
  }

  return (
    <Grid
      container
      direction={'row'}
      justifyContent={'space-between'}
      alignItems={'center'}
      sx={{
        px: 1,
        position: 'relative',
        zIndex: 2,
        backgroundColor: 'background.paper',
        height: headerHeightSize,
        boxShadow: ({ shadows }) => shadows[2]
      }}
    >
      <Grid item xs>
        <WorkspaceSelect />
      </Grid>
      <Grid
        item
        xs
        container
        justifyContent={'center'}
        columnGap={0.7}
        sx={{
          '& > div': {
            height: headerHeightSize,
            width: headerHeightSize,
            backgroundColor: 'primary.main',
            '&  button': { height: '100%', width: '100%' }
          }
        }}
      >
        <Grid item>
          <CustomIconButtonTooltip
            icon={MdPlayArrow}
            onClick={handleRunnerConfig}
            title={'Configurar runner'}
            id={'af6086a5-bce4-4ad1-930b-25131097bd92'}
            iconProps={iconProps}
            tooltipProps={tooltipProps}
          />
        </Grid>
        <Grid item>
          <CustomIconButtonTooltip
            icon={MdInstallDesktop}
            onClick={handleProjectManager}
            title={'Gerenciar projetos'}
            id={'2c140d2d-c4d6-4c16-b23b-8aeafd04aa71'}
            iconProps={iconProps}
            tooltipProps={tooltipProps}
          />
        </Grid>
        <Grid item>
          <CustomIconButtonTooltip
            icon={MdBuild}
            onClick={handleGeneralConfig}
            title={'Configurações'}
            id={'5960a3a1-4674-41dd-8e91-34f11e175904'}
            iconProps={iconProps}
            tooltipProps={tooltipProps}
          />
        </Grid>
      </Grid>
      <Grid item xs></Grid>
    </Grid>
  )
}
