import { Grid, IconButton, Stack, Typography } from '@mui/material'
import { ButtonContainer } from '../ButtonContainer'
import { ApplicationContainer } from '../ApplicationContainer'
import { FaPlus } from 'react-icons/fa'
import { useRunConfiguration } from '@hooks/useRunConfiguration'
import { useNavigate } from 'react-router-dom'

export const MainContainer = () => {
  const navigate = useNavigate()
  const { runConfigurationList } = useRunConfiguration()

  const handleRunnerConfig = () => {
    navigate('/runner-config')
  }

  if (runConfigurationList.length === 0) {
    return (
      <Stack direction={'column'} justifyContent={'center'} alignItems={'center'} sx={{ height: '100%' }}>
        <Typography textAlign={'center'}>Crie uma configuração de projeto</Typography>
        <IconButton onClick={handleRunnerConfig}>
          <FaPlus />
        </IconButton>
      </Stack>
    )
  }

  return (
    <Grid
      container
      direction={'row'}
      wrap={'nowrap'}
      sx={{ height: '100%', overflow: 'hidden', '& > div': { display: 'flex' } }}
    >
      <Grid item>
        <ButtonContainer />
      </Grid>
      <Grid item width={'100%'}>
        <ApplicationContainer />
      </Grid>
    </Grid>
  )
}
