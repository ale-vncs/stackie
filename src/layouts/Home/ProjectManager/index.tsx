import { FormTextField, useFormElement, z } from '@components/FormElement'
import { Container, Grid, Stack, Typography } from '@mui/material'
import { CustomButton, CustomIconButton } from '@components/input'
import { useState } from 'react'
import { BackFrameScreen } from '@components/FrameScreen'
import { useNavigate } from 'react-router-dom'
import { BoxFrame } from '@components/index'
import { useProject } from '@hooks/useProject'
import { FaTimes } from 'react-icons/fa'

interface FormValidGitData {
  gitUrl: string
}

export const ProjectManager = () => {
  const navigate = useNavigate()
  const { loadProject, projects } = useProject()

  const [isValidating, setIsValidating] = useState(false)

  const { control, submit } = useFormElement<FormValidGitData>({
    defaultValues: {
      gitUrl: ''
    },
    validation: z.object({
      gitUrl: z.string().url('Insira uma url válida')
    })
  })

  const clonarProjeto = (gitUrl: string) => {
    window.electron
      .cloneGitProject(gitUrl)
      .then(() => {
        loadProject()
      })
      .finally(() => {
        setIsValidating(false)
      })
  }

  const onSubmit = (data: FormValidGitData) => {
    setIsValidating(true)
    clonarProjeto(data.gitUrl)
  }

  const onClickVoltar = () => {
    navigate('/home')
  }

  return (
    <BackFrameScreen title={'Gerenciar Projeto'} onClickBack={onClickVoltar}>
      <Container>
        <Stack direction={'column'} rowGap={2}>
          <BoxFrame label={'Baixar projeto'}>
            <Grid container direction={'column'} rowGap={2} component={'form'} onSubmit={submit(onSubmit)}>
              <Grid item>
                <FormTextField
                  id={'cfbeebef-d84e-4238-9f5e-dc7d8ec5a944'}
                  control={control}
                  name={'gitUrl'}
                  label={'git url'}
                  fullWidth
                  disabled={isValidating}
                />
              </Grid>
              <Grid item>
                <Stack direction={'row'} columnGap={2} justifyContent={'flex-end'}>
                  <CustomButton id={'55cb5721-6482-49dd-a1b8-a6d34ff27f40'} variant={'outlined'}>
                    Repositório
                  </CustomButton>
                  <CustomButton
                    id={'55cb5721-6482-49dd-a1b8-a6d34ff27f40'}
                    variant={'contained'}
                    isLoading={isValidating}
                    type={'submit'}
                  >
                    Baixar
                  </CustomButton>
                </Stack>
              </Grid>
            </Grid>
          </BoxFrame>
          <BoxFrame label={'Projetos'}>
            <Grid container>
              {projects.map((p) => (
                <Grid key={p.name} item p={1}>
                  <Stack
                    direction={'row'}
                    justifyContent={'space-between'}
                    alignItems={'center'}
                    columnGap={1}
                    sx={({ palette }) => ({ border: `1px solid ${palette.divider}`, p: 1, borderRadius: 1 })}
                  >
                    <Typography>{p.name}</Typography>
                    <CustomIconButton id={'221f64aa-7d9f-444e-81ce-1f3458b0485a'} icon={FaTimes} />
                  </Stack>
                </Grid>
              ))}
            </Grid>
          </BoxFrame>
        </Stack>
      </Container>
    </BackFrameScreen>
  )
}
