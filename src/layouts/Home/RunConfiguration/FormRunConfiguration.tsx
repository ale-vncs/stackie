import { FormRunConfigurationListData } from './useFormRunConfiguration'
import { Stack, Typography } from '@mui/material'
import { FormElementProvider, useFormElement, z } from '@components/FormElement'
import { useFormContext } from 'react-hook-form'
import { useEffect, useMemo } from 'react'
import { RunConfiguration } from '@typings/runConfiguration.typing'
import { runnerValidation } from './validations'
import { ProjectManager } from '@typings/project.typing'
import { FormRunConfigurationNpm } from './FormRunConfiguration/FormRunConfigurationNpm/FormRunConfigurationNpm'
import { FormBase } from './FormRunConfiguration/FormBase'
import { FormValidator } from './FormValidator'

export interface FormRunConfigurationData {
  runner: RunConfiguration | null
}

interface FormRunConfigurationProps {
  runnerIndex: number
}

export const FormRunConfiguration = ({ runnerIndex }: FormRunConfigurationProps) => {
  const { getValues, setValue } = useFormContext<FormRunConfigurationListData>()

  const form = useFormElement<FormRunConfigurationData>({
    defaultValues: {
      runner: null
    },
    validation: z.object({
      runner: runnerValidation
    }),
    validationOnReset: false
  })

  const { watch } = form

  useEffect(() => {
    if (runnerIndex > -1) {
      const runner = getValues('runners')[runnerIndex]
      form.reset({ runner })
    }
  }, [runnerIndex])

  useEffect(() => {
    const w = watch(({ runner }) => {
      if (runner) {
        setValue(`runners.${runnerIndex}`, { ...runner, isValid: runner.isValid ?? false } as RunConfiguration)
      }
    })

    return () => w.unsubscribe()
  }, [watch, runnerIndex])

  const manager = watch('runner.manager')

  const getForm = useMemo(() => {
    if (manager === ProjectManager.NPM) {
      return <FormRunConfigurationNpm />
    }
  }, [manager])

  if (runnerIndex === -1) {
    return (
      <Stack width={'100%'} height={'100%'} justifyContent={'center'} alignItems={'center'}>
        <Typography>Selecione um runner</Typography>
      </Stack>
    )
  }

  return (
    <FormElementProvider formMethods={form}>
      <FormBase>{getForm}</FormBase>
      <FormValidator />
    </FormElementProvider>
  )
}
