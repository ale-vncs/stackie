import { BoxFrame } from '@components/index'
import { Control, useFieldArray, UseFieldArrayRemove } from 'react-hook-form'
import { Box, Grid, Stack, Typography } from '@mui/material'
import { FormTextField } from '@components/FormElement'
import { CustomButton, CustomIconButtonTooltip } from '@components/input'
import { FaMinus } from 'react-icons/fa'
import { FormRunConfigurationData } from '../FormRunConfiguration'

interface EnvFieldProps {
  control: Control<FormRunConfigurationData>
}

interface EnvEditProps extends EnvFieldProps {
  index: number
  remove: UseFieldArrayRemove
}

export const EnvField = ({ control }: EnvFieldProps) => {
  const { fields, append, remove } = useFieldArray({
    control,
    name: 'runner.envList'
  })

  const addEnv = () => {
    append({
      key: '',
      value: ''
    })
  }

  return (
    <BoxFrame label={'Envs'}>
      <Grid container direction={'column'} rowGap={2}>
        {fields.map((f, i) => (
          <Grid item key={f.id}>
            <EnvEdit index={i} control={control} remove={remove} />
          </Grid>
        ))}
      </Grid>
      <Box pt={fields.length ? 2 : 0} />
      <Stack width={'100%'} direction={'row'} justifyContent={'flex-end'}>
        <CustomButton id={'d5906141-c3a2-44c5-9559-829c08631255'} onClick={addEnv} variant={'contained'}>
          Adicionar env
        </CustomButton>
      </Stack>
    </BoxFrame>
  )
}

const EnvEdit = ({ control, index, remove }: EnvEditProps) => {
  return (
    <Grid container direction={'row'} wrap={'nowrap'} columnGap={1} rowGap={2} alignItems={'center'}>
      <Grid item xs>
        <FormTextField
          id={'440eeff6-70cc-442b-81c9-8fbc9dac902a'}
          control={control}
          name={`runner.envList.${index}.key`}
          label={'Chave'}
          fullWidth
        />
      </Grid>
      <Grid item>
        <Typography>=</Typography>
      </Grid>
      <Grid item xs>
        <FormTextField
          id={'440eeff6-70cc-442b-81c9-8fbc9dac902a'}
          control={control}
          name={`runner.envList.${index}.value`}
          label={'Valor'}
          fullWidth
        />
      </Grid>
      <Grid item>
        <CustomIconButtonTooltip
          id={'ef28cb9f-0621-4391-b3be-8ba2bd7e8521'}
          icon={FaMinus}
          title={'Remover'}
          onClick={() => remove(index)}
        />
      </Grid>
    </Grid>
  )
}
