import { Grid } from '@mui/material'
import { FormSelect, FormTextField } from '@components/FormElement'
import { PropsWithChildren } from 'react'
import { useProject } from '@hooks/useProject'
import { ProjectManager } from '@typings/project.typing'
import { EnvField } from './EnvField'
import { useFormContext } from 'react-hook-form'
import { FormRunConfigurationData } from '../FormRunConfiguration'

export const FormBase = ({ children }: PropsWithChildren<unknown>) => {
  const { control, getValues } = useFormContext<FormRunConfigurationData>()
  const { projects } = useProject()

  const options = () => {
    const manager = getValues('runner.manager')
    const adjustPath = (path: string) => {
      if (manager === ProjectManager.NPM) {
        return `${path}/package.json`
      }

      return path
    }

    return projects
      .filter((p) => p.manager === manager)
      .map((p) => ({
        label: adjustPath(p.name),
        value: p.name
      }))
      .flat()
  }

  return (
    <Grid container direction={'column'} wrap={'nowrap'} sx={{ p: 1.5 }} rowGap={2} overflow={'auto'}>
      <Grid item>
        <FormTextField
          id={'ae15b1a5-5dd2-402a-8259-8fddd344af02'}
          control={control}
          name={'runner.name'}
          label={'Nome'}
          fullWidth
        />
      </Grid>
      <Grid item>
        <FormSelect
          options={options()}
          id={'736d2a12-6266-4d9f-8f15-6bfc3173894d'}
          control={control}
          label={'package.json'}
          name={'runner.projectName'}
          fullWidth
        />
      </Grid>
      <Grid item>{children}</Grid>
      <Grid item>
        <FormTextField
          id={'fb36c847-7db9-4e2a-96e9-d617ba68f80f'}
          control={control}
          name={'runner.args'}
          label={'Args'}
          fullWidth
        />
      </Grid>
      <Grid item>
        <EnvField control={control} />
      </Grid>
    </Grid>
  )
}
