import { RunNpmConfiguration } from '@typings/runConfiguration.typing'
import { Grid } from '@mui/material'
import { FormSelect } from '@components/FormElement'
import { useEffect, useState } from 'react'
import { NodeVersionField } from './NodeVersionField/NodeVersionField'
import { useFormContext } from 'react-hook-form'

export const FormRunConfigurationNpm = () => {
  const { control, watch } = useFormContext<{ runner: RunNpmConfiguration }>()

  const [npmScripts, setNpmScripts] = useState<{ label: string; value: string }[]>([])

  const npmCommandOption = () => {
    const commands = ['run', 'install', 'start']
    return commands.map((c) => ({
      label: c,
      value: c
    }))
  }

  const [command, projectName] = watch(['runner.command', 'runner.projectName'])
  const isRun = command === 'run'

  useEffect(() => {
    if (isRun) {
      window.electron.getScriptPackageJson('plataforma-credito-react').then((scripts) => {
        setNpmScripts(scripts.map((s) => ({ label: s, value: s })))
      })
    }
  }, [isRun, projectName])

  return (
    <Grid container direction={'column'} wrap={'nowrap'} rowGap={2}>
      <Grid item>
        <FormSelect
          options={npmCommandOption()}
          id={'99be00e0-d26e-45b2-b7f9-440759aea731'}
          control={control}
          name={'runner.command'}
          fullWidth
          label={'Command'}
        />
      </Grid>
      <Grid item>
        <FormSelect
          options={npmScripts}
          id={'99be00e0-d26e-45b2-b7f9-440759aea731'}
          control={control}
          name={'runner.script'}
          fullWidth
          label={'Script'}
          disabled={!isRun}
        />
      </Grid>
      <Grid item>
        <NodeVersionField control={control} />
      </Grid>
    </Grid>
  )
}
