import { FaCloudDownloadAlt } from 'react-icons/fa'
import { CustomAutoComplete, CustomIconButton } from '@components/input'
import { useEffect, useRef, useState } from 'react'
import { SimpleDialog } from '@components/Dialog/SimpleDialog'
import { useStorage } from '@hooks/useStorage'

interface NodeDownloadProps {
  onClose: () => void
}

export const NodeDownload = ({ onClose }: NodeDownloadProps) => {
  const [openModal, setOpenModal] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const versionSelected = useRef<string | null>()

  const onCloseModal = () => {
    setOpenModal(false)
    onClose()
  }

  const onClickOpenModal = () => {
    setOpenModal(true)
  }

  const onClickDownload = () => {
    if (!versionSelected.current) return
    setIsLoading(true)
    window.electron
      .downloadDevkit('node', versionSelected.current)
      .then((isCompleted) => {
        if (isCompleted) {
          onCloseModal()
          return
        }
      })
      .finally(() => {
        setIsLoading(false)
      })
  }

  return (
    <>
      <CustomIconButton
        id={'eb54956b-c061-41a2-9eff-155b69d2e311'}
        icon={FaCloudDownloadAlt}
        onClick={onClickOpenModal}
      />
      <SimpleDialog
        open={openModal}
        onClose={onCloseModal}
        title={'Baixar Node'}
        maxWidth={'xs'}
        fullWidth
        confirmLabel={'Baixar'}
        onConfirm={onClickDownload}
        loading={isLoading}
      >
        <NodeVersionSelect
          onSelectVersion={(v) => {
            versionSelected.current = v
          }}
        />
      </SimpleDialog>
    </>
  )
}

interface NodeVersionSelectProps {
  onSelectVersion: (version: string | null) => void
}

const NodeVersionSelect = ({ onSelectVersion }: NodeVersionSelectProps) => {
  const [versionsAvailable, setVersionAvailable] = useState<string[]>([])
  const { getData } = useStorage()

  const getVersion = () => {
    getData('managerVersion').then(({ npm }) => {
      setVersionAvailable(npm.available)
    })
  }

  useEffect(() => {
    getVersion()
  }, [])

  return (
    <CustomAutoComplete
      id={'ebd22896-d421-4019-b55e-0d3707a7405d'}
      onChangeValue={onSelectVersion}
      options={versionsAvailable}
    />
  )
}
