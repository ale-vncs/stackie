import { FormSelect } from '@components/FormElement'
import { Stack } from '@mui/material'
import { Control } from 'react-hook-form'
import { RunNpmConfiguration } from '@typings/runConfiguration.typing'
import { useEffect, useState } from 'react'
import { NodeDownload } from './NodeDownload'
import { useStorage } from '@hooks/useStorage'

interface NodeVersionFieldProps {
  control: Control<{ runner: RunNpmConfiguration }>
}

export const NodeVersionField = ({ control }: NodeVersionFieldProps) => {
  const { getData } = useStorage()

  const [nodeVersionList, setNodeVersionList] = useState<string[]>([])

  const getVersions = () => {
    getData('managerVersion').then(({ npm }) => {
      setNodeVersionList(npm.downloaded)
    })
  }

  useEffect(() => {
    getVersions()
  }, [])

  const options = nodeVersionList.map((ver) => ({
    label: ver,
    value: ver
  }))

  return (
    <Stack direction={'row'} columnGap={0.5}>
      <FormSelect
        options={options}
        id={'7d88dd56-abdb-44b6-b277-b9904fe1b734'}
        control={control}
        name={'runner.nodeVersion'}
        fullWidth
        label={'Node'}
      />
      <NodeDownload onClose={getVersions} />
    </Stack>
  )
}
