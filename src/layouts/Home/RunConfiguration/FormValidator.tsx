import { useFormContext } from 'react-hook-form'
import { RunConfiguration } from '@typings/runConfiguration.typing'
import { ProjectManager } from '@typings/project.typing'
import { useEffect } from 'react'
import { npmValidator } from './formValidator/npmValidator'

export const FormValidator = () => {
  const { watch, setValue } = useFormContext<{ runner: RunConfiguration }>()

  const data = watch('runner')

  const validators: Record<typeof data.manager, (data: unknown) => boolean> = {
    [ProjectManager.NPM]: npmValidator,
    [ProjectManager.MAVEN]: () => false
  }

  const commonValidator = () => {
    const valid: boolean[] = []

    valid.push(data.projectName.length > 3)

    return valid.every(Boolean)
  }

  useEffect(() => {
    if (!data.runnerId) return
    setValue('runner.isValid', validators[data.manager](data) && commonValidator())
  }, [data])

  return <></>
}
