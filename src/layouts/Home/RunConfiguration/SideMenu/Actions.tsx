import { Box, ListItemIcon, ListItemText, Menu, MenuItem, Stack } from '@mui/material'
import { CustomIconButton } from '@components/input'
import { FaMinus, FaPlus } from 'react-icons/fa'
import { MouseEventHandler, useState } from 'react'
import { ProjectManager } from '@typings/project.typing'
import { useFieldArray } from 'react-hook-form'
import { FormRunConfigurationListData } from '../useFormRunConfiguration'
import { ulid } from 'ulid'
import { RunnerIcon } from './RunnerIcon'
import { RunNpmConfiguration } from '@typings/runConfiguration.typing'
import { useAppStore } from '@hooks/useRedux'

export const Actions = () => {
  const store = useAppStore()
  const { append } = useFieldArray<FormRunConfigurationListData>({
    name: 'runners'
  })
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement>()

  const openMenuAdd: MouseEventHandler<HTMLButtonElement> = (ev) => {
    setAnchorEl(ev.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(undefined)
  }

  const menus = [
    {
      name: 'npm',
      manager: ProjectManager.NPM
    },
    {
      name: 'maven',
      manager: ProjectManager.MAVEN
    }
  ]

  const getWorkspace = () => {
    return store().system.currentWorkspace
  }

  const createNpmRunner = () => {
    const data: RunNpmConfiguration = {
      runnerId: ulid(),
      manager: ProjectManager.NPM,
      name: 'start',
      script: '',
      command: '',
      projectName: '',
      envList: [],
      args: '',
      workspace: getWorkspace(),
      nodeVersion: '',
      isValid: false
    }
    append(data)
  }

  const onClickMenu = (manager: ProjectManager) => {
    handleClose()
    if (manager === ProjectManager.NPM) {
      createNpmRunner()
      return
    }
  }

  return (
    <Box>
      <Stack direction={'row'} columnGap={0.5}>
        <CustomIconButton id={'4527298a-ec78-4d1e-a36a-235e1f92389d'} icon={FaPlus} onClick={openMenuAdd} />
        <CustomIconButton id={'051c220d-e71e-4c97-8e45-e8da92818343'} icon={FaMinus} />
      </Stack>
      <Menu
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={handleClose}
        slotProps={{ root: { style: { padding: 0 } } }}
      >
        {menus.map((m) => (
          <MenuItem key={m.name} dense onClick={() => onClickMenu(m.manager)}>
            <ListItemIcon>
              <RunnerIcon manager={m.manager} />
            </ListItemIcon>
            <ListItemText primary={m.name} />
          </MenuItem>
        ))}
      </Menu>
    </Box>
  )
}
