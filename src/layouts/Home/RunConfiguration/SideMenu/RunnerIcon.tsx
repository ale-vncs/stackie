import { ProjectManager } from '@typings/project.typing'
import { SiApachemaven, SiNpm } from 'react-icons/si'
import { BsQuestionCircle } from 'react-icons/bs'

interface RunnerIconProps {
  manager: ProjectManager
}

export const RunnerIcon = ({ manager }: RunnerIconProps) => {
  if (manager === ProjectManager.NPM) {
    return <SiNpm />
  }

  if (manager === ProjectManager.MAVEN) {
    return <SiApachemaven />
  }

  return <BsQuestionCircle />
}
