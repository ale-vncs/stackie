import { Divider, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Stack } from '@mui/material'
import { Actions } from './Actions'
import { useFormContext } from 'react-hook-form'
import { FormRunConfigurationListData } from '../useFormRunConfiguration'
import { RunnerIcon } from './RunnerIcon'

export const SideBar = () => {
  const { watch, setValue } = useFormContext<FormRunConfigurationListData>()
  const [runners, runnerSelectedIndex] = watch(['runners', 'runnerSelectedIndex'])

  const onClickItem = (runnerIndex: number) => {
    setValue('runnerSelectedIndex', runnerIndex)
  }

  return (
    <Stack
      direction={'column'}
      width={320}
      flexShrink={0}
      sx={{
        borderRight: ({ palette }) => `1px solid ${palette.divider}`
      }}
    >
      <Actions />
      <Divider flexItem />
      <List sx={{ overflow: 'auto', flex: 1 }}>
        {runners.map((f, i) => (
          <ListItem key={f.runnerId} disablePadding>
            <ListItemButton onClick={() => onClickItem(i)} selected={runnerSelectedIndex === i}>
              <ListItemIcon>
                <RunnerIcon manager={f.manager} />
              </ListItemIcon>
              <ListItemText primary={f.name} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Stack>
  )
}
