import { RunNpmConfiguration } from '@typings/runConfiguration.typing'

export const npmValidator = (data: RunNpmConfiguration) => {
  const valid: boolean[] = []

  valid.push(!!data.nodeVersion)

  if (data.script === 'run') {
    valid.push(!!data.command)
  }

  return valid.every(Boolean)
}
