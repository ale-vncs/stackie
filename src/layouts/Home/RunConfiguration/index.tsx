import { FormRunConfiguration } from './FormRunConfiguration'
import { Grid, Stack } from '@mui/material'
import { SideBar } from './SideMenu/SideBar'
import { FormElementProvider } from '@components/FormElement'
import { useFormRunConfiguration } from './useFormRunConfiguration'
import { CustomButton } from '@components/input'
import { useRunConfiguration } from '@hooks/useRunConfiguration'
import { BackFrameScreen } from '@components/FrameScreen'
import { useNavigate } from 'react-router-dom'

export const RunConfiguration = () => {
  const navigate = useNavigate()
  const { runConfigurationList, setRunConfiguration } = useRunConfiguration()
  const { formMethods } = useFormRunConfiguration(runConfigurationList)

  const runnerSelectedIndex = formMethods.watch('runnerSelectedIndex')

  const handleSaveRunners = () => {
    const runners = formMethods.getValues('runners')
    setRunConfiguration(runners)
  }

  const onClickBack = () => {
    navigate('/home')
  }

  return (
    <BackFrameScreen onClickBack={onClickBack} title={'Configurar Runner'} disablePaddingTop>
      <FormElementProvider formMethods={formMethods}>
        <Grid container direction={'column'} wrap={'nowrap'}>
          <Grid item xs overflow={'hidden'}>
            <Stack direction={'row'} width={'100%'} height={'100%'}>
              <SideBar />
              <FormRunConfiguration runnerIndex={runnerSelectedIndex} />
            </Stack>
          </Grid>
          <Grid item>
            <Stack alignItems={'flex-end'} sx={{ p: 2, borderTop: ({ palette }) => `1px solid ${palette.divider}` }}>
              <CustomButton
                id={'d2e248f6-0295-4b8e-b29c-c0c89e5e511b'}
                variant={'contained'}
                onClick={handleSaveRunners}
              >
                Salvar
              </CustomButton>
            </Stack>
          </Grid>
        </Grid>
      </FormElementProvider>
    </BackFrameScreen>
  )
}
