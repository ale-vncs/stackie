import { useFormElement, z } from '@components/FormElement'
import { RunConfiguration } from '@typings/runConfiguration.typing'
import { runnerValidation } from './validations'
import { useEffect } from 'react'

export interface FormRunConfigurationListData {
  runnerSelectedIndex: number
  runners: RunConfiguration[]
}

export const useFormRunConfiguration = (runners: RunConfiguration[]) => {
  const formMethods = useFormElement<FormRunConfigurationListData>({
    defaultValues: {
      runnerSelectedIndex: -1,
      runners: []
    },
    validation: z.object({
      runnerSelectedIndex: z.number(),
      runners: runnerValidation.array()
    })
  })

  useEffect(() => {
    formMethods.setValue('runners', runners)
  }, [runners])

  return {
    formMethods
  }
}
