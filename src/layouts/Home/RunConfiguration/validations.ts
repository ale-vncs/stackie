import { z } from '@components/FormElement'
import { ProjectManager } from '@typings/project.typing'

const baseValidation = z.object({
  runnerId: z.string(),
  name: z.string(),
  workspace: z.string(),
  projectName: z.string(),
  args: z.string(),
  isValid: z.boolean(),
  envList: z
    .object({
      key: z.string(),
      value: z.string()
    })
    .array()
})

const npmValidation = z
  .object({
    manager: z.literal(ProjectManager.NPM),
    command: z.string(),
    script: z.string(),
    nodeVersion: z.string()
  })
  .merge(baseValidation)

const mavenValidation = z
  .object({
    manager: z.literal(ProjectManager.MAVEN)
  })
  .merge(baseValidation)

export const runnerValidation = z.discriminatedUnion('manager', [npmValidation, mavenValidation])
