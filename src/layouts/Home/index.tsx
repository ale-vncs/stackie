import { Grid } from '@mui/material'
import { Header } from './Header'
import { MainContainer } from './MainContainer'

export const Home = () => {
  return (
    <Grid container height={'100%'} direction={'column'} wrap={'nowrap'}>
      <Grid item>
        <Header />
      </Grid>
      <Grid item overflow={'hidden'} height={'100%'}>
        <MainContainer />
      </Grid>
    </Grid>
  )
}
