import { PropsWithChildren } from 'react'
import { CssBaseline, ThemeProvider } from '@mui/material'
import { useCustomTheme } from '@styles/useCustomTheme'
import { GlobalStyles } from '@styles/GlobalStyles'
import { IconContext } from 'react-icons'

export const CustomThemeProvider = ({ children }: PropsWithChildren<Record<never, never>>) => {
  const theme = useCustomTheme()

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <GlobalStyles />
      <IconContext.Provider value={{ color: theme.palette.text.primary }}>{children}</IconContext.Provider>
    </ThemeProvider>
  )
}
