import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RunConfiguration } from '@typings/runConfiguration.typing'

export enum RunnerStatus {
  NOT_STARTED = 'NOT_STARTED',
  RUNNING = 'RUNNING',
  FINISH = 'FINISH',
  ERROR = 'ERROR'
}

export interface Runner {
  config: RunConfiguration
  status: RunnerStatus
}

export interface RunnerState {
  runners: Runner[]
  runnerIdSelected: string[]
  runnerLogger: Record<string, unknown[]>
}

const initialState: RunnerState = {
  runners: [],
  runnerIdSelected: [],
  runnerLogger: {}
}

const runnerSlice = createSlice({
  name: 'runner',
  initialState,
  reducers: {
    setRunnerIdSelected: (state, action: PayloadAction<RunnerState['runnerIdSelected']>) => {
      state.runnerIdSelected = action.payload
    },
    setRunnersFromRunConfiguration: (state, action: PayloadAction<RunConfiguration[]>) => {
      const runnerIdStateList = state.runners.map((r) => r.config.runnerId)
      action.payload
        .filter((r) => !runnerIdStateList.includes(r.runnerId))
        .forEach((r) => {
          state.runners.push({
            config: r,
            status: RunnerStatus.NOT_STARTED
          })
          state.runnerLogger[r.runnerId] = []
        })
    }
  }
})

export const { setRunnerIdSelected, setRunnersFromRunConfiguration } = runnerSlice.actions

export const runnerReducer = runnerSlice.reducer
