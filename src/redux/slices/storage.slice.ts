import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { ProjectData } from '@typings/project.typing'
import { RunConfiguration } from '@typings/runConfiguration.typing'

export interface StorageState {
  projects: ProjectData[]
  runConfigurations: RunConfiguration[]
}

const initialState: StorageState = {
  projects: [],
  runConfigurations: []
}

const storageSlice = createSlice({
  name: 'project',
  initialState,
  reducers: {
    setProjectList: (state, action: PayloadAction<ProjectData[]>) => {
      state.projects = action.payload
    },
    setRunConfiguration: (state, action: PayloadAction<RunConfiguration[]>) => {
      state.runConfigurations = action.payload
    }
  }
})

export const { setProjectList, setRunConfiguration } = storageSlice.actions

export const storageReducer = storageSlice.reducer
