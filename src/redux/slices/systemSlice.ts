import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export interface SystemState {
  currentWorkspace: string
}

const initialState: SystemState = {
  currentWorkspace: ''
}

const systemSlice = createSlice({
  name: 'system',
  initialState,
  reducers: {
    changeWorkspace: (state, action: PayloadAction<string>) => {
      state.currentWorkspace = action.payload
    }
  }
})

export const { changeWorkspace } = systemSlice.actions
export const systemReducer = systemSlice.reducer
