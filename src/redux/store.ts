import { configureStore } from '@reduxjs/toolkit'
import { systemReducer } from '@redux/slices/systemSlice'
import { storageReducer } from '@slices/storage.slice'
import { runnerReducer } from '@slices/runner.slice'

export const store = configureStore({
  reducer: {
    system: systemReducer,
    storage: storageReducer,
    runner: runnerReducer
  }
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
