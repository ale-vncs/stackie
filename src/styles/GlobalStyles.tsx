import React from 'react'
import { GlobalStyles as GlobalStylesMui, useTheme } from '@mui/material'

export const GlobalStyles = () => {
  const theme = useTheme()

  const scrollbarSize = '0.65em'

  return (
    <GlobalStylesMui
      styles={{
        '*': {
          scrollBehavior: 'smooth'
        },
        body: {
          margin: 0,
          height: '100vh',
          boxSizing: 'border-box',
          webkitFontSmoothing: 'antialiased',
          mozOsxFontSmoothing: 'grayscale',
          '& #root': {
            height: '100%'
          }
        },
        '*::-webkit-scrollbar': {
          width: scrollbarSize
        },
        '*::-webkit-scrollbar-thumb': {
          backgroundColor: theme.palette.primary.light,
          '&:hover': {
            backgroundColor: theme.palette.primary.light
          }
        },
        '*::-webkit-scrollbar:horizontal': {
          height: scrollbarSize
        }
      }}
    />
  )
}
