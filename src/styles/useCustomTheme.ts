import { createTheme, responsiveFontSizes } from '@mui/material'
import { useMemo } from 'react'

export const useCustomTheme = () => {
  return useMemo(
    () =>
      responsiveFontSizes(
        createTheme({
          palette: {
            mode: 'dark',
            primary: {
              main: '#6367C0'
            },
            background: {
              paper: '#323844',
              default: '#282C34'
            },
            divider: '#8F8F8F',
            success: {
              main: '#B8DB87'
            },
            error: {
              main: '#FF5370'
            },
            warning: {
              main: '#F5DE60'
            }
          },
          typography: {
            fontFamily: 'Comfortaa'
          },
          components: {
            MuiDialog: {
              styleOverrides: {
                paper: {
                  backgroundImage: 'unset'
                }
              }
            }
          }
        })
      ),
    []
  )
}
