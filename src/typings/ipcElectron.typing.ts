import { StorageData, StorageDataKey, StorageSetDataReturn } from './storageData.typing'
import { SpawnCommand, SpawnListener } from '@typings/spawn.typing'
import { SpawnSyncReturns } from 'child_process'
import { Devkit, ProjectData } from '@typings/project.typing'

type R = (...arg: unknown[]) => Promise<unknown>
type RVoid = (...arg: unknown[]) => void

export interface IpcAsyncFunction extends Record<string, R> {
  saveData: <K extends StorageDataKey>(key: K, fn: StorageSetDataReturn<K>) => Promise<void>
  getData: <K extends StorageDataKey>(key: K) => Promise<StorageData[K]>
  existFile: (path: string) => Promise<boolean>
  getOperationalSystem: () => Promise<'windows' | 'linux' | 'mac'>
  cloneGitProject: (url: string) => Promise<SpawnSyncReturns<string>>
  loadProject: () => Promise<ProjectData[]>
  getScriptPackageJson: (projectName: string) => Promise<string[]>
  loadDevkitsVersion: () => Promise<void>
  downloadDevkit: (devkit: Devkit, version: string) => Promise<boolean>
}

export interface IpcMainToRenderer extends Record<string, RVoid> {}

export interface IpcRendererToMain extends Record<string, RVoid> {
  spawn: SpawnCommand
}

export interface IpcRendererGenericChannel {
  spawnListener: SpawnListener
}

export type IpcRendererCallback = {
  [key in keyof IpcMainToRenderer]: (cb: (...args: Parameters<IpcMainToRenderer[key]>) => void) => void
}
