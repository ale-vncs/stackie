export type Devkit = 'node' | 'java'

export enum ProjectManager {
  MAVEN = 'maven',
  NPM = 'npm',
  UNKNOWN = 'unknown'
}

export interface ProjectData {
  label: string
  name: string
  gitUrl: string
  path: string
  domain: string
  manager: ProjectManager
}
