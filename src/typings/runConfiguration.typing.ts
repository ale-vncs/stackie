import { ProjectManager } from '@typings/project.typing'

export interface Env {
  key: string
  value: string
}

interface GenericRunConfiguration {
  runnerId: string
  name: string
  workspace: string
  projectName: string
  args: string
  envList: Env[]
  isValid: boolean
}

export interface RunMavenConfiguration extends GenericRunConfiguration {
  manager: ProjectManager.MAVEN
}

export interface RunNpmConfiguration extends GenericRunConfiguration {
  manager: ProjectManager.NPM
  command: string
  script: string
  nodeVersion: string
}

export type RunConfiguration = RunMavenConfiguration | RunNpmConfiguration
