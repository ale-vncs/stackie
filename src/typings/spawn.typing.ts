import { SpawnOptions } from 'child_process'

export type SpawnCommand = (channel: string, config: SpawnRegister) => void

export type SpawnListener = (channel: string, fn: SpawnCallbackFn) => void

type SpawnCallback = (data: string) => void

interface SpawnCallbackFn {
  output?: SpawnCallback
  onClose?: (code: number, err?: unknown) => void
}

export interface SpawnRegister {
  command: string
  args: string[]
  options?: SpawnOptions
}
