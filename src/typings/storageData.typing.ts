import { ProjectData, ProjectManager } from './project.typing'
import { Workspace } from '@typings/workspace.typing'
import { RunConfiguration } from '@typings/runConfiguration.typing'
import { ManagerVersion, NpmManagerVersionData } from '@typings/managerVersion.typing'

export interface GitAccessToken {
  domain: string
  accessToken: string
}

export interface StorageData {
  gitAccessTokenList: GitAccessToken[]
  gitPath: string
  projectList: ProjectData[]
  runConfigurationList: RunConfiguration[]
  workspaceList: Workspace[]
  currentWorkspaceId: string
  windowSize: {
    width: number
    height: number
  }
  managerVersion: {
    npm: ManagerVersion<NpmManagerVersionData>
  }
  systemManagers: {
    manager: ProjectManager
    version: string
    path: string
  }[]
}

export type StorageDataKey = keyof StorageData

export type StorageKeyPair = {
  [key in keyof StorageData]?: StorageData[key]
}

export type StorageSetDataReturn<K extends StorageDataKey> = ((data: StorageData[K]) => StorageData[K]) | StorageData[K]
