import {
  IpcAsyncFunction,
  IpcRendererCallback,
  IpcRendererGenericChannel,
  IpcRendererToMain
} from './ipcElectron.typing'

declare global {
  interface Window {
    electron: IpcAsyncFunction & IpcRendererToMain & IpcRendererCallback & IpcRendererGenericChannel
  }
}

export {}
