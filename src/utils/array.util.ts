import orderBy from 'lodash.orderby'

type Param = Parameters<typeof orderBy>
type Order = Param[2]
type Iteratees<T> = (keyof T)[]

export const orderArrayBy = <T>(collection: T[], iteratees?: Iteratees<T>, orders?: Order) => {
  const oldList = collection.splice(0, collection.length)
  const ordered = orderBy(oldList, iteratees, orders)
  collection.push(...ordered)
}
