import path from 'path'
import { app } from 'electron'
import fs from 'fs'

export const pathUtil = () => {
  const tempPath = path.join(app.getPath('appData'), app.getName(), 'temp')
  const devkit = path.join(app.getPath('appData'), app.getName(), 'data', 'devkit')
  const domainPath = path.join(app.getPath('appData'), app.getName(), 'data', 'projects')
  const nodeVersionPath = path.join(devkit, 'node')

  return {
    domainPath,
    nodeVersionPath,
    tempPath
  }
}

export const createPath = (path: string) => {
  if (!fs.existsSync(path)) {
    fs.mkdirSync(path, { recursive: true })
  }
}
