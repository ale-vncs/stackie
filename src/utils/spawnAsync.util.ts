import { BufferList } from 'bl'
import { spawn } from 'child_process'
import { BufferList as BufferListProps } from 'bl/BufferList'

export const spawnAsync = (...args: Parameters<typeof spawn>) => {
  const child = spawn(...args)
  const stdout = new BufferList()
  const stderr = new BufferList()

  if (child.stdout) {
    child.stdout.on('data', (data) => {
      stdout.append(data)
    })
  }

  if (child.stderr) {
    child.stderr.on('data', (data) => {
      stderr.append(data)
    })
  }

  return new Promise<BufferListProps>((resolve, reject) => {
    child.on('error', reject)

    child.on('close', (code) => {
      if (code === 0) {
        resolve(stdout)
      } else {
        reject({
          code,
          stderr,
          stdout
        })
      }
    })
  })
}
